��    z      �  �   �      H
     I
     [
     b
     j
     q
     u
     {
     �
     �
     �
  .   �
  &     $   )     N  
   T     _     l     {     �  '   �     �     �     �  '   �  	                  #     6     L     Q     `  4   f     �     �     �     �     �  
   �     �     �     �               #  	   ,     6     ?     F  	   K     U     l     r     y     �     �     �     �     �     �     �     �     �     �     �          )     1     L     a     o     �     �     �     �     �     �     �     �     �     �     	               ,     =     Q     Y     k  	   p     z     �  *   �  
   �     �     �     �     �  6   �       
   7     B     R     W     `     p  U   �     �     �     �     �     �          
       %   #     I     N     c     k     �  E  �     �     �     �        	                  (     C     W  5   p  +   �  *   �     �  
                  ,     =  ,   F     s     z     �     �     �     �     �     �  !   �          !     0  :   6      q     �     �     �     �     �     �     �     �            
        "     +     D     J     Q     g     z     �     �     �     �     �     �     �     �     �                    *     A     ^     g     �     �     �     �     �     �     �     �          #  	   *     4     D  
   U     `     h     v     �  	   �     �     �  
   �     �     �  -   �     '     8     H     X     _  1   f     �     �     �     �     �     �       a   !     �  	   �     �  
   �     �  	   �     �     �  #   �     
          "     )     ?     c   4   [           x      L                 F   P      	   3       =               d              I   /   h   #       H   Y   6   a       N          A   7          G   B          u      k   q          <   ,   >   j   E   `   ?   0   v   O   ^         i       Q   f   ]   '   $   S          J       Z   \   o   D                         W      %   &   9   b   t              "   _           p       l   e   X   V   :   
          R   2       5   K   z   -       w       )      y   T   U       g           8   s       *   C      1      !   r          n              M       (             m      +             .   @   ;    Account not found Action Actions Active Add Admin Admin / Profile /  Admin / Settings /  Admin / Users /  An Internal Error Has Occurred. An error occurred on password reset. Try again An error occurred on remove. Try again An error occurred on save. Try again Angle Annotation Apply shadow Bits Allocated Bits Stored Cancel Check your email to reset your password Clear Column Pixel Spacing Columns Confirm Password not equals to password Dashboard Decode Time Description Description in use Description not blank Edit Elliptical ROI Email Enter the information below to recover your password Enter your user and e-mail Eraser Error Error on find table Exams Exams view Export Forget Password Forget password Freeform ROI Gender High Bit Highlight Image ID Length List Load Time Logged in successfully Login Logout Module Name Non-existent Non-existent User Nonexistent Not Set E-mail Not Set User Number Of Frames Pan Password Password is empty Password recover Password reset successfully Patient Photometric Interpretation Pixel Representation Pixel Spacing Planar Configuration Probe Profile Profile in use Profile not blank Profile permissions Profile too short Profiles Recover Recover password Rectangle ROI Remember me Remove Repeat password RescaleIntercept Reset your password Restore Row Pixel Spacing Rows SOP Class Samples Per Pixel Save Security token invalid. Generate new token Select all Setting Settings State Studies The requested address %s was not found on this server. This account is disabled. Total Time Transfer Syntax User Username Username in use Username is empty Username or password not found, please try again or make sure your account is enabled Username too short Users Value View WW/WC Welcome WindowCenter WindowWidth You do not have access to this module Zoom created successfully profile removed successfully updated successfully Project-Id-Version: PROJECT VERSION
PO-Revision-Date: 2018-08-24 16:50-0300
Language-Team: LANGUAGE <EMAIL@ADDRESS>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
POT-Creation-Date: 
X-Generator: Poedit 2.1.1
Last-Translator: 
Language: pt
 Conta não encontrada Ação Ações Ativo Adicionar Admin Admin / Perfil /  Admin / Configurações /  Admin / Usuário /  Um erro interno ocorreu. Um erro ocorreu ao redefinir a senha. Tente novamente Um erro ocorreu ao excluir. Tente novamente Um erro ocorreu ao salvar. Tente novamente Angulo Anotação Aplicar sombra Bits alocados Bits armazenados Cancelar Verifique seu email para redefinir sua senha Limpar Espaço entre colunas de pixel Colunas Senhas não são iguais Painel Tempo de decodificação Descrição Descrição em uso Descrição não pode estar vazio Editar Área eliptica Email Entre com as informações abaixo para redefinir sua senha Entre com o seu usuário e email Borracha Erro Erro ao encontrar a tabela Exames Visualização de exames Exportar Recuperar senha Esqueci a senha Área livre Genero Bits altos Destacar Identificação de Image Medir Listar Tempo de carregamento Logado com sucesso Entrar Sair Modulo Nome Não existente Usuário não existe Não existente Email não informado Usuário não informado Núm. De frames Mover Senha Senha não pode estar vazio Recuperação de senha Senha redefinida com sucesso Paciente Interpretação photometrica Representação de piel Espaço do pixel Configuração planar Ponto Perfil Perfil em uso Perfil não pode estar vazio Permissões do peril Perfil muito curto Perfil Recuperar Redefinir senha Área retangular Lembrar-me Remover Repetir senha Interceptação de escala Redefina sua senha Restaurar Espaço entre linhas de pixel Linhas Classe SOP Amostragem por Pixel Salvar Token de segurança invalido. Gere novo token Selecionar todos Configurações Configurações Estado Estudo O endereço %s não foi encontrado neste servidor Esta conta está desabilitada Tempo total Sintaxe de transferência Usuário Usuário Usuário já em uso Usuário não pode estar vazio Usuário ou senha não encontrados, por favor tente novamente ou verifique se a conta está ativa Usuário muito curto Usuários Valor Visualizar WW/WC Bem Vindo Centro de janela Largura de janela Você não tem acesso a este modulo Zoom criado com sucesso perfil excluído com sucesso atualizado com sucesso 