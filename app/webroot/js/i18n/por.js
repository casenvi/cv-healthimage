var lang = "por";
exam = {
    StudyDescription:   "Descrição",
    StudyDate:          "Data",
    seriesTotal:        "Total de series",
    SeriesNumber:       "Número da Series",
    SeriesDescription:  "Descrição da serie",
    imageTotal:         "Total de imagens",
    StudyInstitution:   "Unidade",
    StudyBody:          "Parte do corpo",
    Save:               "Salvar",
    Cancel:             "Cancelar",
    EnterAnnotation:    "Entre com sua anotação",
    Remove:             "Remover",
    EditAnnotation:     "Editar sua anotação",
    StateName:          "Nome do status",
    StateError:         "Ocorreu um erro ao salvar. Tente novamente."
};
list = {
    lengthMenu:         "Mostrar _MENU_ registros por página",
    zeroRecords:        "Desculpe, nenhum dado encontrado",
    info:               "Mostrando página _PAGE_ de _PAGES_",
    infoEmpty:          "Nenhum registro encontrado",
    infoFiltered:       "(Filtrado de _MAX_ registros)",
    first:              "Primeiro",
    last:               "Último",
    next:               "Próximo",
    previous:           "Anterior",
    processing:         "Processando...",
    search:             "Procurar",
    loadingRecords:     "Carregando ...",
    decimal:            ",",
    thousands:          '.',
    copyTitle:          "Copiado para area de transferência",
    copySuccess_:       "%d registros copiados",
    copySuccess1:       "1 registro copiado"
}