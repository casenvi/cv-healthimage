var lang = "eng";
exam = {
    StudyDescription:   "StudyDescription",
    StudyDate:          "StudyDate",
    seriesTotal:        "seriesTotal",
    SeriesNumber:       "SeriesNumber",
    SeriesDescription:  "SeriesDescription",
    imageTotal:         "imageTotal",
    StudyInstitution:   "Institution",
    StudyBody:          "Body",
    Save:               "Save",
    Cancel:             "Cancel",
    EnterAnnotation:    "Enter your annotation",
    Remove:             "Remove",
    EditAnnotation:     "Edit your annotation",
    StateName:          "State Name",
    StateError:         "There was an error updating as information. Try again."
};
list = {
    lengthMenu:         "Show _MENU_ entires",
    zeroRecords:        "Nothing found - sorry",
    info:               "Showing page _PAGE_ of _PAGES_",
    infoEmpty:          "No records available",
    infoFiltered:       "(filtered from _MAX_ total records)",
    first:              "First",
    last:               "Last",
    next:               "Next",
    previous:           "Previous",
    processing:         "Processing",
    search:             "Search",
    loadingRecords:     "Loading ...",
    decimal:            ".",
    thousands:          ',',
    copyTitle:          "Copied to clipboard",
    copySuccess_:       "%d records copied",
    copySuccess1:       "1 record copied"
}