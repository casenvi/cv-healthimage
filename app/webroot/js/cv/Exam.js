var tablePatient;
var tableStudy;

window.alert = function() {};

function getExams(){
    var data = [];
    $.ajax({
        url: "/Exams/getExams",
        type: "GET",
        success: function (resp) {
            resp = JSON.parse(resp);
            if (!$.isEmptyObject(resp)) {
                // data processing
                for (row in resp) {
                    for (row2 in resp[row]) {
                        if ('Patient' === row2) {
                            // add patient
                            var tmp = {};
                            tmp['name'] = resp[row][row2]['firstname'];
                            tmp['nasc'] = formatDate(resp[row][row2]['birthdate']);
                            tmp['mrecord'] = resp[row][row2]['medical_record'];
                            tmp['lastexam'] = formatDate(resp[row][row2]['lastexam']);
                            tmp['lastinstitution'] = resp[row][row2]['lastinstitution'];
                            tmp['studyTotal'] = 0;
                            tmp ['studies'] = [];
                            // add studies
                            for ($studies in resp[row][row2]['Study']) {
                                var study = {};
                                study['StudyID'] = resp[row][row2]['Study'][$studies]['Study']['study_uid'];
                                study['StudyDescription'] = resp[row][row2]['Study'][$studies]['Study']['study_desc'];
                                study['StudyDate'] = formatDate(resp[row][row2]['Study'][$studies]['Study']['appt_date']);
                                study['StudyInstitution'] = resp[row][row2]['Study'][$studies]['Study']['institution'];
                                study['StudyBody'] = resp[row][row2]['Study'][$studies]['Image'][0]['body_part_examined'];
                                study['images'] = []; // array of images
                                study['imagesTotal'] = 0;
                                tmp['studies'].push(study);
                                tmp['studyTotal']++;

                                //add image
                                for ($images in resp[row][row2]['Study'][$studies]['Image']) {
                                    var image = {};
                                    image['SOPInstanceUID'] = resp[row][row2]['Study'][$studies]['Image'][$images]['SOPInstanceUID'];
                                    image['src'] = resp[row][row2]['Study'][$studies]['Image'][$images]['src'];
                                    image['id'] = resp[row][row2]['Study'][$studies]['Image'][$images]['id'];
                                    image['thumb'] = resp[row][row2]['Study'][$studies]['Image'][$images]['thumb'];
                                    study['images'].push(image);
                                    study['imagesTotal']++;
                                }
                            }
                            //add to array
                            data.push(tmp);
                        }
                    }
                }
                // short by date desc
                data = sortByKey(data, 'lastexam');
                // tablePatient processing
                tablePatient = $('#list_ajax').DataTable({
                    "language": {
                        "lengthMenu": list.lengthMenu,
                        "zeroRecords": list.zeroRecords,
                        "info": list.info,
                        "infoEmpty": list.infoEmpty,
                        "infoFiltered": list.infoFiltered,
                        "paginate": {
                            "first": list.first,
                            "last": list.last,
                            "next": list.next,
                            "previous": list.previous
                        },
                        "processing": list.processing,
                        "search": list.search,
                        "loadingRecords": list.loadingRecords,
                        "decimal": list.decimal,
                        "thousands": list.thousands,
                        buttons: {
                            copyTitle: list.copyTitle,
                            copySuccess: {
                                _: list.copySuccess_,
                                1: list.copySuccess1
                            }
                        }
                    },
                    data: data,
                    columns: [
                        {
                            "className": 'details-patient',
                            "orderable": false,
                            "data": null,
                            "defaultContent": ''
                        },
                        {data: "name"},
                        {data: "nasc"},
                        {data: "mrecord"},
                        {data: "lastexam"},
                        {data: "lastinstitution"},
                        {data: "studyTotal"}
                    ],
                    retrieve: true
                });

                // make tableStudy
                $('#list_ajax tbody').on('click', 'td.details-patient', function () {
                    var tr = $(this).closest('tr');
                    var row = tablePatient.row(tr);

                    if (row.child.isShown()) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    }
                    else {
                        // Open this row
                        row.child(formatStudy()).show();
                        tr.addClass('shown');
                        tableStudy = $('#patient-details').DataTable({
                            "language": {
                                "lengthMenu": list.lengthMenu,
                                "zeroRecords": list.zeroRecords,
                                "info": list.info,
                                "infoEmpty": list.infoEmpty,
                                "infoFiltered": list.infoFiltered,
                                "paginate": {
                                    "first": list.first,
                                    "last": list.last,
                                    "next": list.next,
                                    "previous": list.previous
                                },
                                "processing": list.processing,
                                "search": list.search,
                                "loadingRecords": list.loadingRecords,
                                "decimal": list.decimal,
                                "thousands": list.thousands,
                                buttons: {
                                    copyTitle: list.copyTitle,
                                    copySuccess: {
                                        _: list.copySuccess_,
                                        1: list.copySuccess1
                                    }
                                }
                            },
                            data: row.data().studies,
                            columns: [
                                {
                                    "className": 'details-study',
                                    "orderable": false,
                                    "data": null,
                                    "defaultContent": ''
                                },
                                {data: 'StudyDescription'},
                                {data: 'StudyDate'},
                                {data: 'StudyInstitution'},
                                {data: 'StudyBody'},
                                {data: 'imagesTotal'}
                            ],
                            retrieve: true,
                            paging: false,
                            ordering: false,
                            info: false,
                            searching: false
                        });
                        // tableImage
                        $('#patient-details tbody').on('click', 'td.details-study', function () {
                            var tr = $(this).closest('tr');
                            var row = tableStudy.row(tr);

                            if (row.child.isShown()) {
                                // This row is already open - close it
                                row.child.hide();
                                tr.removeClass('shown');
                            }
                            else {
                                // Open this row
                                var t = row.data().images;
                                var r = formatImage(t);
                                row.child(r).show();
                                tr.addClass('shown');
                            }
                        });
                    }
                });
            }
        }
    });
}

function formatDate(udate){
    switch (lang) {
        case 'por':
            ret = udate.substring(8, 10) + '-';
            ret += udate.substring(5, 7) + '-';
            ret += udate.substring(0, 4);
            break;
        default:
            ret = udate;
            break;
    }
    return ret;
}

function in_array (needle, haystack, key = null, level = 1) {
    var theIndex = -1;
    if (null === key){
        switch (level){
            case 1:
                if ($.isArray(haystack)) {
                    for (var i = 0; i < haystack.length; i++) {
                        if (haystack[i] == needle) {
                            theIndex = i;
                            break;
                        }
                    }
                }
                break;

            case 2:
                if ($.isArray(haystack)) {
                    for (var i = 0; i < haystack.length; i++) {
                        if ($.isArray(haystack[i])) {
                            for (var k = 0; k < haystack[i].length; k++) {
                                if (haystack[i][k] == needle) {
                                    theIndex = i;
                                    break;
                                }
                            }
                        }
                    }
                }
                break;
            case 3:
                if ($.isArray(haystack)) {
                    for (var i = 0; i < haystack.length; i++) {
                        if ($.isArray(haystack[i])) {
                            for (var k = 0; k < haystack[i].length; k++) {
                                if ($.isArray(haystack[i][k])) {
                                    for (var m = 0; m < haystack[i][k].length; m++) {
                                        if (haystack[i][k][m] == needle) {
                                            theIndex = i;
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
                break;
        }
    } else{
        switch (level){
            case 1:
                if ($.isArray(haystack)) {
                    if (haystack[key] == needle) {
                        theIndex = true;
                        break;
                    }
                }
                break;

            case 2:
                if ($.isArray(haystack)) {
                    for (var i = 0; i < haystack.length; i++) {
                        if (haystack[i][key] == needle) {
                            theIndex = i;
                            break;
                        }
                    }
                }
                break;
            case 3:
                if ($.isArray(haystack)) {
                    for (var i = 0; i < haystack.length; i++) {
                        if ($.isArray(haystack[i])) {
                            for (var k = 0; k < haystack[i].length; k++) {
                                if ($.isArray(haystack[i][k])) {
                                    if (haystack[i][k][key] == needle) {
                                        theIndex = i;
                                        break;
                                    }
                                }
                            }
                        }

                    }
                }
                break;
        }
    }


    return theIndex;
}

function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x > y) ? -1 : ((x < y) ? 1 : 0));
    });
}

function formatStudy ( ) {
    var ret = '<table id="patient-details" class="table table-striped table-bordered table-hover" cellpadding="5"'
            + 'cellspacing="0" style="padding-left:50px;">';
    ret += '<thead><tr><th></th><th>' + exam.StudyDescription + '</th>'
        + '<th>' + exam.StudyDate + '</th>'
        + '<th>' + exam.StudyInstitution + '</th>'
        + '<th>' + exam.StudyBody + '</th>'
        + '<th>'  + exam.imageTotal + '</th></tr></thead>';
    ret += '<tbody></tbody></table>';
    return ret;
}

function formatImage ( d ) {
    var ret = '<table id="details-study" cellpadding="5" cellspacing="0" style=" width: 100%;"><tr>';
    for (row in d){
        if (0 === (row % 3)){
            ret+='</tr><tr>'
        }
        ret+= '<td><img onclick="modal(\'' + d[row]['src'] + '\', \'' + d[row]['id'] + '\' );" src="' + d[row]['thumb'] + '" style=" width: 33%;"></td>';
    }
    ret += '</tr></table>';
    return ret;
}

// function to show exame

function loadAndViewImage(imageId, ID) {
    if (imageId == ""){
        alert("imageId not set");
        exit();
    }
    var element = document.getElementById('dicomImage');
    try {
        cornerstone.enable(element);
        var start = new Date().getTime();
        console.log(imageId);
        cornerstone.loadImage(imageId).then(function(image) {
            // clean to select
            $('#savedStates')
                .find('option')
                .remove()
                .end();

            var viewport = cornerstone.getDefaultViewportForImage(element, image);
            var config = {
                getTextCallback : getTextCallback,
                changeTextCallback : changeTextCallback,
                drawHandles : false,
                drawHandlesOnHover : true,
                arrowFirst : true
            }
            cornerstone.displayImage(element, image, viewport);
            if(loaded === false) {
                cornerstoneTools.mouseInput.enable(element);
                //cornerstoneTools.touchInput.enable(element);
                cornerstoneTools.mouseWheelInput.enable(element);
                cornerstoneTools.wwwc.activate(element, 4);
                cornerstoneTools.pan.activate(element, 1);
                cornerstoneTools.zoom.activate(element, 2);
                cornerstoneTools.zoomWheel.activate(element);
                cornerstoneTools.rotate.activate(element); // rotate image
                // Enable all tools we want to use with this element
                //cornerstoneTools.probe.enable(element);
                cornerstoneTools.length.enable(element);
                cornerstoneTools.ellipticalRoi.enable(element);
                cornerstoneTools.rectangleRoi.enable(element);
                cornerstoneTools.angle.enable(element);
                cornerstoneTools.eraser.enable(element);

                // Try commenting this out to see the default behaviour
                // By default, the tool uses Javascript's Prompt function
                // to ask the user for the annotation. This example uses a
                // slightly nicer HTML5 dialog element.
                cornerstoneTools.arrowAnnotate.setConfiguration(config);

                // Enable all tools we want to use with this element
                cornerstoneTools.arrowAnnotate.enable(element, 1);
                cornerstoneTools.arrowAnnotateTouch.enable(element);

                // set the stack as tool state
                cornerstoneTools.addStackStateManager(element, ['stack']);


                loaded = true;
                activate("enableWindowLevelTool");
            }

            function activate(id) {
                document.querySelectorAll('a').forEach(function(elem) {
                    elem.classList.remove('active');
                });

                document.getElementById(id).classList.add('active');
            }

            // helper function used by the tool button handlers to disable the active tool
            // before making a new tool active
            function disableAllTools()
            {
                cornerstoneTools.wwwc.disable(element);
                cornerstoneTools.pan.activate(element, 2); // 2 is middle mouse button
                cornerstoneTools.zoom.activate(element, 4); // 4 is right mouse button
                cornerstoneTools.probe.deactivate(element, 1);
                cornerstoneTools.length.deactivate(element, 1);
                cornerstoneTools.ellipticalRoi.deactivate(element, 1);
                cornerstoneTools.rectangleRoi.deactivate(element, 1);
                cornerstoneTools.angle.deactivate(element, 1);
                cornerstoneTools.highlight.deactivate(element, 1);
                cornerstoneTools.rotate.deactivate(element, 1);
                cornerstoneTools.freehand.deactivate(element, 1);
                cornerstoneTools.eraser.deactivate(element, 1);
                cornerstoneTools.arrowAnnotate.deactivate(element, 1);
            }

            // Tool button event handlers that set the new active tool
            document.getElementById('enableWindowLevelTool').addEventListener('click', function() {
                activate('enableWindowLevelTool')
                disableAllTools();
                cornerstoneTools.wwwc.activate(element, 1);
            });
            document.getElementById('pan').addEventListener('click', function() {
                activate('pan')
                disableAllTools();
                cornerstoneTools.pan.activate(element, 3); // 3 means left mouse button and middle mouse button
            });
            document.getElementById('zoom').addEventListener('click', function() {
                activate('zoom')
                disableAllTools();
                cornerstoneTools.zoom.activate(element, 5); // 5 means left mouse button and right mouse button
            });
            document.getElementById('enableLength').addEventListener('click', function() {
                activate('enableLength')
                disableAllTools();
                cornerstoneTools.length.activate(element, 1);
            });
            document.getElementById('probe').addEventListener('click', function() {
                activate('probe')
                disableAllTools();
                cornerstoneTools.probe.activate(element, 1);
            });
            document.getElementById('circleroi').addEventListener('click', function() {
                activate('circleroi')
                disableAllTools();
                cornerstoneTools.ellipticalRoi.activate(element, 1);
            });
            document.getElementById('rotate').addEventListener('click', function() {
                activate('rotate')
                disableAllTools();
                cornerstoneTools.rotate.activate(element, 1);
            });
            document.getElementById('rectangleroi').addEventListener('click', function() {
                activate('rectangleroi')
                disableAllTools();
                cornerstoneTools.rectangleRoi.activate(element, 1);
            });
            document.getElementById('angle').addEventListener('click', function () {
                activate('angle')
                disableAllTools();
                cornerstoneTools.angle.activate(element, 1);
            });
            document.getElementById('highlight').addEventListener('click', function() {
                activate('highlight')
                disableAllTools();
                cornerstoneTools.highlight.activate(element, 1);
            });
            document.getElementById('annotation').addEventListener('click', function() {
                activate("annotation");
                disableAllTools();
                cornerstoneTools.arrowAnnotate.activate(element, 1);
                cornerstoneTools.arrowAnnotateTouch.activate(element);
            });
            document.getElementById('freehand').addEventListener('click', function() {
                activate('freehand')
                disableAllTools();
                cornerstoneTools.freehand.activate(element, 1);
            });
            document.getElementById('eraser').addEventListener('click', function() {
                activate('eraser');
                disableAllTools();

                // In order for the eraser to work, other tools must be in the 'enable'
                // state. This allows eraser to receive mouse click events on other tools'
                // data.
                cornerstoneTools.probe.enable(element, 1);
                cornerstoneTools.length.enable(element, 1);
                cornerstoneTools.ellipticalRoi.enable(element, 1);
                cornerstoneTools.rectangleRoi.enable(element, 1);
                cornerstoneTools.angle.enable(element, 1);
                cornerstoneTools.highlight.enable(element, 1);
                cornerstoneTools.freehand.enable(element, 1);
                cornerstoneTools.eraser.enable(element, 1);

                cornerstoneTools.eraser.activate(element, 1);
            });

            document.getElementById('export').addEventListener('click', function() {
                //var filename = document.getElementById('filename').value;
                cornerstoneTools.saveAs(element, imageId + ".png");
                return false;
            });

            document.getElementById('restore').addEventListener("click",function(){
                activate("restore");
                var select = document.getElementById("savedStates");
                var serializedState = select.value;
                var appState = JSON.parse(serializedState);
                cornerstoneTools.appState.restore(appState);
            });

            function getTransferSyntax() {
                const value = image.data.string('x00020010');
                return value + ' [' + uids[value] + ']';
            }

            function getSopClass() {
                const value = image.data.string('x00080016');
                return value + ' [' + uids[value] + ']';
            }

            function getPixelRepresentation() {
                const value = image.data.uint16('x00280103');
                if(value === undefined) {
                    return;
                }
                return value + (value === 0 ? ' (unsigned)' : ' (signed)');
            }

            function getPlanarConfiguration() {
                const value = image.data.uint16('x00280006');
                if(value === undefined) {
                    return;
                }
                return value + (value === 0 ? ' (pixel)' : ' (plane)');
            }


// image.data.uint16('x');

            document.getElementById('patient').textContent = image.data.string('x00100010');
            document.getElementById('medical_record').textContent = image.data.string('x00100020');
            document.getElementById('imageID').textContent = ID;
            //document.getElementById('transferSyntax').textContent = getTransferSyntax();
            //document.getElementById('sopClass').textContent = getSopClass();
            document.getElementById('samplesPerPixel').textContent = image.data.uint16('x00280002');
            document.getElementById('photometricInterpretation').textContent = image.data.string('x00280004');
            document.getElementById('numberOfFrames').textContent = image.data.string('x00280008');
           // document.getElementById('planarConfiguration').textContent = getPlanarConfiguration();
            document.getElementById('rows').textContent = image.data.uint16('x00280010');
            document.getElementById('columns').textContent = image.data.uint16('x00280011');
            document.getElementById('pixelSpacing').textContent = image.data.string('x00280030');
            document.getElementById('rowPixelSpacing').textContent = image.rowPixelSpacing;
            document.getElementById('columnPixelSpacing').textContent = image.columnPixelSpacing;
            document.getElementById('bitsAllocated').textContent = image.data.uint16('x00280100');
            document.getElementById('bitsStored').textContent = image.data.uint16('x00280101');
            document.getElementById('highBit').textContent = image.data.uint16('x00280102');
            document.getElementById('pixelRepresentation').textContent = getPixelRepresentation();
            document.getElementById('windowCenter').textContent = image.data.string('x00281050');
            document.getElementById('windowWidth').textContent = image.data.string('x00281051');
            document.getElementById('rescaleIntercept').textContent = image.data.string('x00281052');
            //document.getElementById('rescaleSlope').textContent = image.data.string('x00281053');
            //document.getElementById('basicOffsetTable').textContent = image.data.elements.x7fe00010.basicOffsetTable ? image.data.elements.x7fe00010.basicOffsetTable.length : '';
            //document.getElementById('fragments').textContent = image.data.elements.x7fe00010.fragments ? image.data.elements.x7fe00010.fragments.length : '';
            //document.getElementById('minStoredPixelValue').textContent = image.minPixelValue;
            //document.getElementById('maxStoredPixelValue').textContent = image.maxPixelValue;
            var end = new Date().getTime();
            var time = end - start;
            document.getElementById('totalTime').textContent = time + "ms";
            document.getElementById('loadTime').textContent = image.loadTimeInMS + "ms";
            document.getElementById('decodeTime').textContent = image.decodeTimeInMS + "ms";

            //Update saved states
            getStates(ID);


        }, function(err) {
            alert(err.error);
        });
        cornerstone.resize(element, true);
    }
    catch(err) {
        alert(err.message);
    }
}

// Define a callback to get your text annotation
// This could be used, e.g. to open a modal
function getTextCallback(doneChangingTextCallback) {
    bootbox.prompt({
        title: exam.EnterAnnotation,
        buttons: {
            confirm: {
                label: exam.Save,
                className: 'btn-success'
            },
            cancel: {
                label: exam.Cancel,
                className: 'btn-danger'
            }
        },
        callback: function(result) {
            if (result == null){
                doneChangingTextCallback(null, undefined, true);
            }else {
                doneChangingTextCallback(result);
            }
        }
    });

}

function changeTextCallback(data, eventData, doneChangingTextCallback) {
    bootbox.prompt({
        title: exam.EditAnnotation,
        value: data.text,
        buttons: {
            confirm: {
                label: exam.Save,
                className: 'btn-success'
            },
            cancel: {
                label: exam.Remove,
                className: 'btn-danger'
            }
        },
        callback: function(result){
            if (result == null){
                doneChangingTextCallback(data, undefined, true);
            }else{
                doneChangingTextCallback(data, result);
            }

        }
    });
}

function timeNow() {
    var d = new Date(),
        h = (d.getHours()<10?'0':'') + d.getHours(),
        m = (d.getMinutes()<10?'0':'') + d.getMinutes(),
        s = (d.getMinutes()<10?'0':'') + d.getSeconds();
    return h + ':' + m + ':' + s;
}

function getStates(img){
    $.get(
        "/ViewStates/getViews/" + img,
        null,
        function (j) {
            var data = JSON.parse(j);
            if (!$.isEmptyObject(data)){
                data.forEach(function (item, index) {
                    var select = document.getElementById("savedStates");
                    var newOption = document.createElement("option");
                    newOption.value = item.state;
                    newOption.text = item.description;
                    select.appendChild(newOption);
                });
            }
        }
    );
}

function setStates(id){
    bootbox.prompt({
        'title': exam.StateName,
        buttons: {
            confirm: {
                label: exam.Save,
                className: 'btn-success'
            },
            cancel: {
                label: exam.Cancel,
                className: 'btn-danger'
            }
        },
        callback: function(result) {
            if (result != null){
                result = result + " - " + timeNow();
                var element = document.getElementById(id);
                var appState = cornerstoneTools.appState.save([element]);
                var serializedState = JSON.stringify(appState);
                var data = {
                    image_id: document.getElementById('imageID').textContent,
                    description: result,
                    state: serializedState
                };
                $.post("/ViewStates/setViews/", data)
                    .done(function(f) {
                        var select = document.getElementById("savedStates");
                        var newOption = document.createElement("option");
                        newOption.value = serializedState;
                        newOption.text = result;
                        select.appendChild(newOption);
                        bootbox.alert (f);
                    })
                    .fail(function() {
                        bootbox.alert (exam.StateError);
                    });
            }
        }
    })

}
