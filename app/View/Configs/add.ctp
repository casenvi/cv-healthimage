
<!-- #page specific plugin scripts  -->
<div class="page-header">
    <h3>Settings</h3>
</div><!-- /.page-header -->
<div class="row">
    <div class="col-xs-12">
        <div class="clearfix"></div>
        <?php
        echo $this->Form->create('Config', array(
            'role' => 'form'));
        ?>
        <div class='row'>
            <div class="col-md-4">
                <?php
                echo $this->Form->input('description', array(
                    'label' => array(
                        'text' => __('Description'),
                        'class' => 'control-label'
                    ),
                    'data-step' => '1',
                    'class' => 'form-control'
                ));
                ?>
            </div>
            <div class="col-md-4">
                <?php
                echo $this->Form->input('value', array(
                    'label' => array(
                        'text' => __('Value'),
                        'class' => 'control-label'
                    ),
                    'type' => 'textarea',
                    'maxlength' => 1024,
                    'data-step' => '1',
                    'class' => 'form-control'
                ));
                ?>
            </div>
        </div>
        <div class='clearfix form-actions left'>
            <div class="col-md-12">
                <?php
                echo $this->Html->link(
                        __('Cancel'), array(
                    'action' => 'index'), array(
                    'escape' => false,
                    'class' => 'btn btn-danger'
                        )
                );
                echo $this->Form->end(array(
                    'label' => __('Save'),
                    'class' => 'btn btn-success',
                    'div' => false
                ));
                ?>
            </div>
        </div>
    </div>
</div>
