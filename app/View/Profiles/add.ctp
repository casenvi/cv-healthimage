<?php
echo $this->Html->css(array(
    'cv/various.css'
));
?>
<script>
    $(document).ready(function () {
        $('#permission').DataTable({
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Desculpe, nenhum dado encontrado",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "Nenhum registro encontrado",
                "infoFiltered": "(Filtrado de _MAX_ registros)",
                "paginate": {
                    "first": "Primeiro",
                    "last": "Último",
                    "next": "Próximo",
                    "previous": "Anterior"
                },
                "processing": "Procesando...",
                "search": "Procurar:",
                "loadingRecords": "Carregando...",
                "decimal": ",",
                "thousands": ".",
            },
            "searching": true,
            "ordering": false,
            "paginate": false,
            "info": false
        });
    });
    function toggle(source, dest) {
        if (source.checked) {
            $(dest).each(function () {
                this.checked = true;
            });
        } else {
            $(dest).each(function () {
                this.checked = false;
            });
        }
    }
</script>
<div class="page-header">
    <h3><?= __('Profiles');?></h3>
</div><!-- /.page-header -->
<div class="row">
    <div class="col-xs-12">
        <div class="clearfix"></div>
        <?php
        echo $this->Form->create('Profile', array(
            'role' => 'form'));
        ?>
        <div class='row'>
            <div class="col-md-8">
                <?php
                echo $this->Form->input('name', array(
                    'label' => 'Nome',
                    'class' => 'form-control',
                    'data-step' => '1'
                ));
                ?>
            </div>
            <div class="col-md-1" style="margin-top: 30px">
                <label>
                    <input type="hidden" name="data[Profile][status]" value="0" />
                    <input type="checkbox"  value="1" <?php
                    if (isset($this->request->data['Profile']['status'])) {
                        echo 'checked=""';
                    } else if (!isset($this->request->data['Profile'])) {
                        echo 'checked=""';
                    }
                    ?> name="data[Profile][status]" value="1" class="ace" />
                    <span class="lbl"> <?= __('Active');?></span>
                </label>
            </div>
            <?php
            echo $this->Form->input('id', array(
                'type' => 'hidden'
            ));
            echo $this->Form->input('created_by', array(
                'type' => 'hidden',
                'value' => $uid
            ));
            ?>
        </div>
    </div>
</div>
<div class="page-header">
    <h4><?= __('Profile permissions');?></h4>
</div>
<div class='row'>        
    <div class="col-md-1">
    </div>
    <div class="col-md-10">
        <div class="clearfix">
            <table class="table-ajax table table-striped table-hover table-bordered" data-step="3" id='permission' >
                <thead>
                    <tr>
                        <th><?= __('Module');?></th>
                        <th style="text-align: center">
                            <?= __('Add');?><br>
                            <input type="checkbox" onClick="toggle(this, '.add')" /><?= __('Select all');?>
                        </th>
                        <th style="text-align: center">
                            <?= __('Edit');?><br>
                            <input type="checkbox"  onClick="toggle(this, '.edit')" /><?= __('Select all');?>
                        </th>
                        <th style="text-align: center">
                            <?= __('Remove');?><br>
                            <input type="checkbox"  onClick="toggle(this, '.del')" /> <?= __('Select all');?>
                        </th>
                        <th style="text-align: center">
                            <?= __('View');?><br>
                            <input type="checkbox"  onClick="toggle(this, '.show1')" /><?= __('Select all');?>
                        </th>
                    </tr>
                </thead>
                <tbody>         
                    <?php
                    foreach ($modules as $key => $value) {
                        $i = 0;
                        ?>
                        <tr>
                            <td><?php
                            echo $key;
                            ?></td>
                            <?php
                            if (in_array('Adicionar', $value)) {
                                $id = array_search('Adicionar', $value);
                                ?>    
                                <td style="text-align: center"><input type="checkbox"
                                                                      name="data[Profile][Module][]"
                                                                      class="add"
                                                                      value="<?php echo $id; ?>"
                                                                      <?php
                                                                      foreach ($this->request->data['Module'] as $selected) {
                                                                          if (in_array($id, $selected)) {
                                                                              echo 'checked = "checked"';
                                                                          }
                                                                      }
                                                                      ?>
                                                                      >
                                </td>                                    
                            <?php } else {
                                ?>  <td style="text-align: center"><input type="checkbox" disabled="disabled"></td> <?php
                            }
                            if (in_array(__('Edit'), $value)) {
                                $id = array_search(__('Edit'), $value);
                                ?>    
                                <td style="text-align: center"><input type="checkbox"
                                                                      name="data[Profile][Module][]" 
                                                                      class ="edit"
                                                                      value="<?php echo $id; ?>"
                                                                      <?php
                                                                      foreach ($this->request->data['Module'] as $selected) {
                                                                          if (in_array($id, $selected)) {
                                                                              echo 'checked = "checked"';
                                                                          }
                                                                      }
                                                                      ?>
                                                                      >
                                </td>                                                
                            <?php } else {
                                ?>  <td style="text-align: center"><input type="checkbox"  disabled="disabled"></td> <?php
                            }
                            if (in_array('Excluir', $value)) {
                                $id = array_search('Excluir', $value);
                                ?>    
                                <td style="text-align: center"><input type="checkbox" 
                                                                      name="data[Profile][Module][]",
                                                                      class="del"
                                                                      value="<?php echo $id; ?>"
                                                                      <?php
                                                                      foreach ($this->request->data['Module'] as $selected) {
                                                                          if (in_array($id, $selected)) {
                                                                              echo 'checked = "checked"';
                                                                          }
                                                                      }
                                                                      ?>
                                                                      >
                                </td>                                                
                            <?php } else {
                                ?>  <td style="text-align: center"><input type="checkbox" disabled="disabled"></td> <?php
                            }
                            if (in_array('Visualizar', $value)) {
                                $id = array_search('Visualizar', $value);
                                ?>    
                                <td style="text-align: center"><input type="checkbox"  
                                                                      name="data[Profile][Module][]" 
                                                                      class="show1"
                                                                      value="<?php echo $id; ?>"
                                                                      <?php
                                                                      foreach ($this->request->data['Module'] as $selected) {
                                                                          if (in_array($id, $selected)) {
                                                                              echo 'checked = "checked"';
                                                                          }
                                                                      }
                                                                      ?>
                                                                      >
                                </td>        
                            <?php } else {
                                ?>  <td style="text-align: center"><input type="checkbox" disabled="disabled"></td> <?php
                    }
                            ?>                        
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table> 
            <br>
        </div>
    </div>
    <div class="col-md-1">
    </div> 
</div>
<hr />
<div class='clearfix form-actions left'>
    <div class="col-md-12">
        <?php
        echo $this->Html->link(
                __('Cancel'), array(
            'action' => 'index'), array(
            'escape' => false,
            'class' => 'btn btn-danger'
                )
        );
        echo $this->Form->end(array(
            'label' => __('Save'),
            'class' => 'btn btn-success',
            'div' => false
        ));
        ?>
    </div>
</div>
</div>
</div>