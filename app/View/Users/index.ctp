<?php
echo $this->Html->css(array(
    'datatables.net/css/jquery.dataTables.css',
));
//page specific plugin scripts
echo $this->Html->script(array(
    'datatables.net/jquery.dataTables.js',
    'datatables.net-buttons/dataTables.buttons.js',
    'datatables.net-buttons/buttons.flash.js',
    'datatables.net-buttons/buttons.html5.js',
    'datatables.net-buttons/buttons.print.js',
    'datatables.net-buttons/buttons.colVis.js',
    'datatables.net-select/dataTables.select.js',
    'cv/list.js'
));
?>
<!-- #section: Remove Confirmation -->
<script>
    function del($link) {
        bootbox.confirm("<p>Você está prestes a remover um registro, esse procedimento é irreversível.</p>\n\
                            <p>Você deseja prosseguir?</p>", function (e) {
            if (e) {
                window.open($link, "_self");
            }
        });
    }
</script>
<!-- /section: Remove Confirmation -->
<div class="page-header">
    <h3><?= __('Users');?></h3>
</div><!-- /.page-header -->

<div class="row">
    <div class="col-xs-12">
        <div class="clearfix">
            <div class="pull-left tableTools">
                <?php
                echo $this->Html->link("<i class='fas fa-plus-circle' aria-hidden='true'></i>", array(
                    'controller' => 'Users',
                    'action' => 'add'), array(
                    'class' => 'dt-button buttons-collection buttons-colvis btn btn-white btn-primary btn-bold',
                    'title' => __('Add'),
                    'data-step' => '1',
                    'escape' => false
                ));
                ?>
            </div>
            <div class="pull-right tableTools-container">

            </div>
        </div>
        <div class="table-header">
            <br/>
        </div>
        <div class="space-6"></div>
        <div>
            <table class="table table-striped table-bordered table-hover" data-step="3" id="list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th><?= __('User');?></th>
                        <th><?= __('Profile');?></th>
                        <th><?= __('Action');?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 0;
                    foreach ($retorno as $value):
                        $i++;
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $value['User']['username']; ?></td>
                            <td><?php echo $value['Profile']['name']; ?></td>
                            
                            <td>
                                <div class="hidden-sm hidden-xs action-buttons">
                                    <?php
                                    echo $this->Html->link(
                                            '<i class="ace-icon fas fa-pencil-alt bigger-130"></i>', array(
                                        'controller' => 'Users',
                                        'action' => 'add',
                                        $value['User']['id']), array(
                                        'escape' => false,
                                        'class' => 'green',
                                        'title' => __('Edit'),
                                        'id' => 'edit'
                                    ));
                                    ?>
                                    <a class="red" title="" id="remove" 
                                       onclick="<?php echo "del('Users/remove/" . $value['User']['id'] . "')"; ?>" 
                                       data-original-title="Remover" >
                                        <i class="ace-icon fas fa-trash-alt bigger-130"></i>
                                    </a>
                                </div>
                                <!-- section menu mobile -->
                                <div class="hidden-md hidden-lg">
                                    <div class="inline pos-rel">
                                        <button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                            <i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
                                        </button>

                                        <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                            <li>
                                                <?php
                                                echo $this->Html->link(
                                                        '<i class="ace-icon fas fa-pencil-alt bigger-130"></i>', array(
                                                    'controller' => 'Users',
                                                    'action' => 'add',
                                                    $value['User']['id']), array(
                                                    'escape' => false,
                                                    'class' => 'green',
                                                    'title' => __('Edit'),
                                                    'id' => 'edit'
                                                ));
                                                ?>
                                            </li>

                                            <li>
                                                <a class="red" title="" id="remove" 
                                                   onclick="<?php echo "del('Users/remove/" . $value['User']['id'] . "')"; ?>" 
                                                   data-original-title="Remover" >
                                                    <i class="ace-icon fas fa-trash-alt bigger-130"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>