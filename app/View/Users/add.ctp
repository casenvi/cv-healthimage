<!-- page specific plugin scripts  -->
    <?php
echo $this->Html->script(array(
    'select2/select2.full.min.js'
));
echo $this->Html->css(array(
    'select2/select2_custon.min.css',
    'cv/various.css'
));
?>
<!-- inline java script -->
<script>
    $(document).ready(function () {
        // select_find
        $(".selfind").select2();
        // # end select_find
    });
</script>
<!-- #page specific plugin scripts  -->
<div class="page-header">
    <h3><?=__('Users'); ?></h3>
</div><!-- /.page-header -->
<div class="row">
    <div class="col-xs-12">
        <div class="clearfix"></div>
        <?php
        echo $this->Form->create('User', array(
            'role' => 'form'));
        ?>
        <div class='row'>
            <div class="col-md-3">
                <?php
                echo $this->Form->input('username', array(
                    'label' => array(
                        'text' => __('User'),
                        'class' => 'control-label'
                    ),
                    'data-step' => '1',
                    'class' => 'form-control'
                ));
                ?>
            </div>            
            <div class="col-md-2">
                <?php
                echo $this->Form->input('profile_id', array(
                    'label' => __('Profile'),
                    'class' => 'form-control selfind',
                    'data-step' => '3',
                    'options' => $ProfilesList
                ));
                ?>
            </div>
            <div class="col-md-2">
                <?php
                echo $this->Form->input('language', array(
                    'label' => __('Language'),
                    'class' => 'form-control selfind',
                    'data-step' => '3',
                    'options' => $LanguageList
                ));
                ?>
            </div>
            <div class="col-md-2">
                <?php
                echo $this->Form->input('password', array(
                    'tabindex' => '4',
                    'required' => false,
                    'label' => __('Password'),
                    'class' => 'form-control',
                    'data-step' => '3',
                ));
                ?>
            </div>
            <div class="col-md-2">
                <?php
                echo $this->Form->input('confirma', array(
                    'tabindex' => '5',
                    'required' => false,
                    'class' => 'form-control',
                    'label' => __('Repeat password'),
                    'type' => 'password',
                    'data-step' => '4',
                ));
                echo $this->Form->input('id', array(
                    'type' => 'hidden'
                ));
                echo $this->Form->input('created_by', array(
                    'type' => 'hidden',
                    'value' => $uid
                ));
                ?>
            </div>
            <div class="col-md-1" style="margin-top: 30px">
                <label>
                    <input type="checkbox"  value="1" checked="" name="data[User][status]" class="ace" />
                    <span class="lbl"> <?= __('Active'); ?></span>
                </label>
            </div>
        </div>
        <div class='clearfix form-actions left'>
            <div class="col-md-12">
                <?php
                echo $this->Html->link(
                        __('Cancel'), array(
                    'action' => 'index'), array(
                    'escape' => false,
                    'class' => 'btn btn-danger'
                        )
                );
                echo $this->Form->end(array(
                    'label' => __('Save'),
                    'class' => 'btn btn-success',
                    'div' => false
                ));
                ?>
            </div>
        </div>
    </div>
</div>
