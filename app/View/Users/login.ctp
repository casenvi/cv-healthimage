<div class="main-container">
    <div class="main-content">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="login-container">
                    <div class="center">
                        <div class="space-32"></div>
                        <div class="space-32"></div>
                        <img src="/img/marca-pmf.svg">
                        <h3>
                            <span class="red">Health Image</span>
                            <span class="grey" id="id-text2"></span>
                        </h3>
                    </div>

                    <div class="space-6"></div>

                    <div class="position-relative">
                        <div id="login-box" class="login-box visible widget-box no-border">
                            <div class="widget-body">
                                <div class="widget-main">
                                    <h4 class="header blue lighter bigger">
                                        <i class="ace-icon fa fa-sign-in green"></i>
                                        <?= __('Login'); ?>
                                    </h4>

                                    <div class="space-6"></div>
                                    <?php
                                    echo $this->Form->create('User', array(
                                        'class' => 'form-signin'
                                    ));
                                    ?>
                                    <fieldset>
                                        <label class="block clearfix">
                                            <span class="block input-icon input-icon-right">
                                                <input type="text" name="data[User][username]" id="username"
                                                       class="form-control" placeholder="<?= __('User');?>" />
                                                <i class="ace-icon fa fa-user"></i>
                                            </span>
                                        </label>

                                        <label class="block clearfix">
                                            <span class="block input-icon input-icon-right">
                                                <input type="password" name="data[User][password]" id="password"
                                                       class="form-control" placeholder="<?= __('Password');?>" />
                                                <i class="ace-icon fa fa-lock"></i>
                                            </span>
                                        </label>

                                        <div class="space"></div>

                                        <div class="clearfix">
                                            <label class="inline">
                                                <input type="hidden" name="data[User][rememberMe]" value="0"/>
                                                <input type="checkbox"  value="1" name="data[User][rememberMe]"
                                                       value="1" id="rememberMe" class="ace" />
                                                <span class="lbl"> <?= __('Remember me');?></span>
                                            </label>
                                            <?php
                                            echo $this->Form->button('<i class="ace-icon fa fa-key"></i>
                                                            <span class="bigger-110">' . __('Login') . '</span>', array(
                                                'type' => 'submit',
                                                'class' => 'width-35 pull-right btn btn-sm btn-primary'
                                            ));
                                            ?>
                                        </div>

                                        <div class="space-4"></div>
                                    </fieldset>
                                    </form>

                                    <div class="space-6"></div>

                                </div><!-- /.widget-main -->

                                <div class="toolbar clearfix">
                                    <div>
                                        
                                        <?php
                                        echo $this->Form->end();
                                        ?>
                                    </div>

                                </div>
                            </div><!-- /.widget-body -->
                        </div><!-- /.login-box -->

                        <div id="forgot-box" class="forgot-box widget-box no-border">
                            <div class="widget-body">
                                <div class="widget-main">
                                    <h4 class="header red lighter bigger">
                                        <i class="ace-icon fa fa-key"></i>
                                        <?= __('Forget password');?>
                                    </h4>

                                    <div class="space-6"></div>
                                    <p>
                                        <?= __('Enter your user and e-mail');?>
                                    </p>

                                    <?php
                                    echo $this->Form->create('User', array(
                                        'class' => 'form-signin',
                                        'url' => 'forgetpwd'
                                    ));
                                    ?>
                                    <fieldset>
                                        <label class="block clearfix">
                                            <span class="block input-icon input-icon-right">
                                                <?php
                                                echo $this->Form->input('username', array(
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'placeholder' => __('User')
                                                ));
                                                ?>

                                                <i class="ace-icon fa fa-user"></i>
                                            </span>
                                        </label>
                                        <label class="block clearfix">
                                            <span class="block input-icon input-icon-right">
                                                <?php
                                                echo $this->Form->input('email', array(
                                                    'label' => false,
                                                    'class' => 'form-control',
                                                    'placeholder' => __('Email')
                                                ));
                                                ?>
                                                <i class="ace-icon fa fa-envelope"></i>
                                            </span>
                                        </label>

                                        <div class="clearfix">
                                            <?php
                                             echo $this->Form->button('<i class="ace-icon fa fa-lightbulb-o"></i>
                                                <span class="bigger-110">' . __('Recover') . '</span>',array(
                                                    'type'=>'submit',
                                                    'class'=>'width-35 pull-right btn btn-sm btn-danger'
                                                 ));
                                            echo $this->Form->end();
                                            ?>
                                        </div>
                                    </fieldset>
                                    </form>
                                </div><!-- /.widget-main -->

                                <div class="toolbar center">
                                    <a href="#" data-target="#login-box" class="back-to-login-link">
                                        Voltar
                                        <i class="ace-icon fa fa-arrow-right"></i>
                                    </a>
                                </div>
                            </div><!-- /.widget-body -->
                        </div><!-- /.forgot-box -->
                    </div><!-- /.position-relative -->
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.main-content -->
</div><!-- /.main-container -->