<?php echo $this->Html->css(array('signin')); ?>
<div class='row-fluid'>
    <div class="col-md-2">
    </div>
    <div class="col-md-8">
        <?php    
            echo $this->Form->create('User',array(
                'class'=>'form-signin', 
                'action' => 'forgetpwd'
            )); 
            echo $this->Session->flash(); 
        ?>
        <div style="text-align:center">
            <?php echo $this->Html->image('logo125x78.jpg', array(
                'alt' => 'Logo' 
                ));?>
            <h2 class="form-signin-heading"><?= __('Password recover');?></n></h2>
            <h5><?= __('Enter the information below to recover your password');?></h5>
        </div>
    <?php 
        echo $this->Form->input('username',array(
            'label'=> array(
                    'text' => __('Username'),
                    'class' => 'sr-only'
                ),
                'class'=>'form-control',
                'placeholder'=> __('Username')
        ));
        echo $this->Form->input('email',array(
            'label'=> array(
                    'text' => __('Email'),
                    'class' => 'sr-only'
                ),
                'class'=>'form-control',
                'placeholder'=> __('Email')
        ));
        echo $this->Form->button(__('Recover password'),array(
                'type'=>'submit',
                'class'=>'btn btn-lg btn-primary btn-custom btn-block'
             ));
        echo $this->Form->end();
    ?>
</div>
    <div class="col-md-2">
    </div>
</div>