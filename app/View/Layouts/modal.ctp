<div class="modal-dialog modal-lg" style="background-color: #e9e9e9">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="title"><?php echo $title; ?></h4>
        </div>
        <div class="modal-body modal-body-scroll">
            <?php echo $this->fetch('content'); ?>
        </div>
        <?php if (isset($footer)) { ?>
            <div class="modal-footer">
                <?php echo $footer; ?>
            </div>
        <?php } ?>
    </div>
</div>