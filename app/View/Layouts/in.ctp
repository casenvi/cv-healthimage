<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title>PMF - Health Image</title>
        <?php
        // time zone
        date_default_timezone_set('America/Sao_Paulo');
        // # time zone
        ?>

        <meta name="description" content="overview &amp; stats" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

        <!-- i18n javascript -->
        <?php
        switch ($this->Session->read('Config.language')){
            case 'por':
                echo $this->Html->script('i18n/por.js');
                break;
            default:
                echo $this->Html->script('i18n/eng.js');
                break;
        }
        ?>

        <!-- bootstrap & fontawesome -->
        <?php
        echo $this->Html->css(array(
            'bootstrap/bootstrap.css',
            'fonts/font-awesome-5-all.min.css',
        ));
        ?>
        <!-- page specific plugin styles -->

        <!-- text fonts / styles-->
        <?php
        echo $this->Html->css(array(
            'ace/ace-fonts.css',
            'ace/ace.css'
        ));
        ?>
        <!--[if lte IE 9]>
        <?php
        echo $this->Html->css(array(
            'ace/ace-part2.css'
        ));
        ?>
<![endif]-->
        <?php
        echo $this->Html->css(array(
            'ace/ace-rtl.css',
            'ace/ace-skins.css',
        ));
        ?>

        <!--[if lte IE 9]>
        <?php
        echo $this->Html->css(array(
            'ace/ace-ie.css'
        ));
        ?>		  
        <![endif]-->

        <!-- inline styles related to this page -->

        <!-- ace settings handler -->
        <?php
        echo $this->Html->script(array(
            'ace/ace-extra.js',
        ));
        ?>

        <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

        <!--[if lte IE 8]>
        <?php
        echo $this->Html->script(array(
            'html5shiv/dist/html5shiv.min.js',
            'respond/dest/respond.min.js'
        ));
        ?><![endif]-->
        <!-- basic scripts -->

        <!--[if !IE]> -->
        <?php
        echo $this->Html->script(array(
            'jquery/dist/jquery.js'
        ));
        ?>

        <!-- <![endif]-->

        <!--[if IE]>

        <?php
        echo $this->Html->script(array(
            'jquery.1x/dist/jquery.js'
        ));
        ?>
<![endif]-->     
        <script type="text/javascript">
            if ('ontouchstart' in document.documentElement)
                document.write("<script src='/js/jquery.mobile.custom/jquery.mobile.custom.js'>" + "<" + "/script>");
        </script>
        <?php
        echo $this->Html->script(array(
            'bootstrap/bootstrap.js'
        ));
        ?>

        <!-- page specific plugin scripts -->

        <!--[if lte IE 8]>
        <?php
        echo $this->Html->script(array(
            'ExplorerCanvas/excanvas.js'
        ));
        ?>
        <![endif]-->
        <?php
        echo $this->Html->script(array(
            'jquery-ui.custom/jquery-ui.custom.js',
            'jqueryui-touch-punch/jquery.ui.touch-punch.js'
        ));
        ?>

        <!-- ace scripts -->
        <?php
        echo $this->Html->script(array(
            'src/elements.scroller.js',
            'src/elements.colorpicker.js',
            'src/elements.fileinput.js',
            'src/elements.typeahead.js',
            'src/elements.wysiwyg.js',
            'src/elements.spinner.js',
            'src/elements.treeview.js',
            'src/elements.wizard.js',
            'src/elements.aside.js',
            'src/ace.js',
            'src/ace.basics.js',
            'src/ace.scrolltop.js',
            'src/ace.ajax-content.js',
            'src/ace.touch-drag.js',
            'src/ace.sidebar.js',
            'src/ace.sidebar-scroll-1.js',
            'src/ace.submenu-hover.js',
            'src/ace.widget-box.js',
            'src/ace.settings.js',
            'src/ace.settings-rtl.js',
            'src/ace.settings-skin.js',
            'src/ace.widget-on-reload.js',
            'bootbox.js/bootbox.js',
            'src/ace.searchbox-autocomplete.js'
        ));
        ?>
    </head>

    <body class="no-skin">
        <?php
        // Notifications
        echo $this->Html->script(array(
            'notify/notify.min.js'
        ));
        echo $this->Session->flash();
        ?>
        <!-- #section:Modal Group
        <div id="modais" class="modal fade"></div>
        <!-- /section:Modal Group -->
        <!-- #section:ProgressBar -->
        <?php
        echo $this->Html->css(array(
            'progressbar/progressbar.css'
        ));
        echo $this->Html->script(array(
            'progressbar/progressbar.js'
        ));
        ?>

        <!-- /section:ProgressBar -->
        <!-- #section:basics/navbar.layout -->
        <div id="navbar" class="navbar navbar-default          ace-save-state">
            <div class="navbar-container ace-save-state" id="navbar-container">
                <!-- #section:basics/sidebar.mobile.toggle -->
                <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
                    <span class="sr-only">Toggle sidebar</span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>
                </button>

                <!-- /section:basics/sidebar.mobile.toggle -->
                <div class="navbar-header pull-left">
                    <!-- #section:basics/navbar.layout.brand -->
                    <a href="#" class="navbar-brand">
                        <img src="/img/marca-pmf.svg" style="width: 160px">
                    </a>

                    <!-- /section:basics/navbar.layout.brand -->

                    <!-- #section:basics/navbar.toggle -->

                    <!-- /section:basics/navbar.toggle -->
                </div>

                <!-- #section:basics/navbar.dropdown -->
                <div class="navbar-buttons navbar-header pull-right" role="navigation">
                    <ul class="nav ace-nav">
                        <!-- #section:basics/navbar.user_menu -->
                        <li class="light-blue dropdown-modal">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                <span class="user-info">
                                    <small><?= __('Welcome');?> , </small>
                                    <?php echo $user['User']['username']; ?>
                                </span>

                                <i class="ace-icon fa fa-caret-down"></i>
                            </a>

                            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                            '<i class="ace-icon fa fa-cog"></i>' . __('Profile'), array('controller' => 'Users', 'action' => 'add', $user['User']['id']), array(
                                        'escape' => false,
                                        'class' => 'animated animated-short fadeInUp'));
                                    ?>
                                </li>
                                <li class="divider"></li>
                                <!-- logout -->
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                            '<i class="ace-icon fa fa-power-off"></i>' . __('Logout'), array('controller' => 'Users', 'action' => 'logout'), array(
                                        'escape' => false,
                                        'class' => 'animated animated-short fadeInUp'));
                                    ?>
                                </li>
                            </ul>
                        </li>

                        <!-- /section:basics/navbar.user_menu -->
                    </ul>
                </div>

                <!-- /section:basics/navbar.dropdown -->
            </div><!-- /.navbar-container -->
        </div>

        <!-- /section:basics/navbar.layout -->
        <div class="main-container ace-save-state" id="main-container">
            <script type="text/javascript">
                try {
                    ace.settings.loadState('main-container')
                } catch (e) {
                }
            </script>

            <!-- #section:basics/sidebar -->
            <div id="sidebar" class="sidebar responsive ace-save-state">
                <script type="text/javascript">
                    try {
                        ace.settings.loadState('sidebar')
                    } catch (e) {
                    }
                </script>

                <!-- Start: Sidebar Menu -->
                <?php echo $this->element('menu'); ?>
                <!-- End: Sidebar Menu -->


                <!-- #section:basics/sidebar.layout.minimize -->
                <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
                    <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
                </div>

                <!-- /section:basics/sidebar.layout.minimize -->
            </div>

            <!-- /section:basics/sidebar -->
            <div class="main-content">
                <div class="main-content-inner">
                    <!-- #section:basics/content.breadcrumbs -->
                    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                        <ul class="breadcrumb">
                            <li>
                                <i class="ace-icon fa fa-home home-icon"></i>  <?php echo $way; ?>
                        </ul><!-- /.breadcrumb -->
                    </div>

                    <!-- /section:basics/content.breadcrumbs -->
                    <div class="page-content">
                        <!-- PAGE CONTENT BEGINS -->
                        <?php
                        echo $this->fetch('content');
                        ?>
                        <!-- PAGE CONTENT ENDS -->
                    </div><!-- /.page-content -->
                </div>
                <div class="hidden-lg">
                    <br><br><br><br><br><br>
                </div>
            </div><!-- /.main-content -->

            <div class="footer">
                <div class="footer-inner">
                    <!-- #section:basics/footer -->
                    <div class="footer-content">
                    </div>

                    <!-- /section:basics/footer -->
                </div>
            </div>

            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->
    </body>
</html>

