<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title>PMF - Health Image</title>

        <meta name="description" content="Página de Entrada" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

        <!-- bootstrap & fontawesome -->
        <?php
        echo $this->Html->css(array(
            'bootstrap/bootstrap.css',
            'fonts/font-awesome.css'
        ));
        ?>

        <!-- text fonts / styles-->
        <?php
        echo $this->Html->css(array(
            'ace/ace-fonts.css',
            'ace/ace.css'
        ));
        ?>

        <!--[if lte IE 9]>
        <?php
        echo $this->Html->css(array(
            'ace/ace-part2.css'
        ));
        ?>
        <![endif]-->
        <?php
        echo $this->Html->css(array(
            'ace/ace-rtl.css'
        ));
        ?>

        <!--[if lte IE 9]>
        <?php
        echo $this->Html->css(array(
            'ace/ace-ie.css'
        ));
        ?>		  
        <![endif]-->

        <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

        <!--[if lte IE 8]>
        <?php
        echo $this->Html->script(array(
            'html5shiv/dist/html5shiv.min.js',
            'respond/dest/respond.min.js'
        ));
        ?>
        <![endif]-->
        
    </head>

    <body class="login-layout skin-2 light-login">
        <?php 
        
        echo $this->fetch('content');        
        ?>
         <!-- basic scripts -->

        <!--[if !IE]> -->
        <?php
        echo $this->Html->script(array(
            'jquery/dist/jquery.js'
        ));
        ?>

        <!-- <![endif]-->

        <!--[if IE]>

        <?php
        echo $this->Html->script(array(
            'jquery.1x/dist/jquery.js'
        ));
        ?>
<![endif]-->        
        <script type="text/javascript">
            if ('ontouchstart' in document.documentElement)
                document.write("<script src='js/jquery.mobile.custom/jquery.mobile.custom.js'>" + "<" + "/script>");
        </script>
        <!-- inline scripts related to this page -->
        <script type="text/javascript">
            jQuery(function ($) {
                $(document).on('click', '.toolbar a[data-target]', function (e) {
                    e.preventDefault();
                    var target = $(this).data('target');
                    $('.widget-box.visible').removeClass('visible');//hide others
                    $(target).addClass('visible');//show target
                });
            });



            //you don't need this, just used for changing background
            jQuery(function ($) {
                $('#btn-login-dark').on('click', function (e) {
                    $('body').attr('class', 'login-layout');
                    $('#id-text2').attr('class', 'white');
                    $('#id-company-text').attr('class', 'blue');

                    e.preventDefault();
                });
                $('#btn-login-light').on('click', function (e) {
                    $('body').attr('class', 'login-layout light-login');
                    $('#id-text2').attr('class', 'grey');
                    $('#id-company-text').attr('class', 'blue');

                    e.preventDefault();
                });
                $('#btn-login-blur').on('click', function (e) {
                    $('body').attr('class', 'login-layout blur-login');
                    $('#id-text2').attr('class', 'white');
                    $('#id-company-text').attr('class', 'light-blue');

                    e.preventDefault();
                });

            });
        </script>
        
        <?php
        echo $this->Html->script(array(
            'notify/notify.min.js'            
        ));
         echo $this->Session->flash();
        ?>

       
    </body>
</html>