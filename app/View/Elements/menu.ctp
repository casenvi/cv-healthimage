<ul class="nav nav-list">
    <!-- Dashboard -->
    <li <?php
    if ($active == 'Dashboard')
        echo ' class="active"';
    ?> >
        <?php
        echo $this->Html->link(
            '<i class="menu-icon fas fa-tachometer-alt"></i><span class="menu-text">' . __('Dashboard') . '</span>', array(
            'controller' => 'Pages',
            'action' => 'dashboard'
        ), array(
                'escape' => false,
            )
        );
        ?>
        <b class="arrow"></b>
    </li>
    <!-- Exams -->
    <li <?php
    if ($active == 'Exams')
        echo ' class="active"';
    ?> >
        <?php
        echo $this->Html->link(
            '<i class="menu-icon fas fa-briefcase-medical"></i><span class="menu-text">' . __('Exams') .'</span>', array(
            'controller' => 'Exams',
            'action' => 'index'
        ), array(
                'escape' => false,
            )
        );
        ?>
        <b class="arrow"></b>
    </li>
    <!-- Admin -->
    <li<?php
    if ($active == 'Users_list' || $active == 'Users_add' ||
            $active == 'Profiles_list' || $active == 'Profiles_add'||
            $active == 'Configs_list' || $active == 'Configs_add') {
        echo ' class="open"';
    }
    ?> >
        <a class="dropdown-toggle" href="#">
            <span class="menu-icon fa fa-cogs"></span>
            <span class="menu-text"> <?= __('Admin'); ?> </span>
            <b class="arrow fas fa-angle-down"></b>
        </a>
        <ul class="submenu">
            <!-- Users -->
            <li<?php
            if ($active == 'Users_list' || $active == 'Users_add') {
                echo ' class="open"';
            }
            ?> >
                <a class="dropdown-toggle" href="#">
                    <span class="fas fa-key"></span> <?= __('Users'); ?>
                    <b class="arrow fas fa-angle-down"></b>
                </a>
                <!-- thrid level -->
                <ul class="submenu">        
                    <li <?php
                    if ($active == 'Users_add')
                        echo ' class="active"';
                    ?> >
                            <?php
                            echo $this->Html->link(
                                    '<span class="glyphicon glyphicon-edit"></span>' . __('Add'), array(
                                'controller' => 'Users',
                                'action' => 'add'), array(
                                'escape' => false,
                                'class' => 'animated animated-short fadeInUp')
                            );
                            ?>
                    </li>
                    <li <?php
                    if ($active == 'Users_list')
                        echo ' class="active"';
                    ?> >
                            <?php
                            echo $this->Html->link(
                                    '<span class="glyphicon glyphicon-modal-window"></span>' . __('List'), array(
                                'controller' => 'Users',
                                'action' => 'index'
                                    ), array(
                                'escape' => false,
                                'class' => 'animated animated-short fadeInUp')
                            );
                            ?>
                    </li><!-- End third level -->
                </ul> 
            </li><!-- End second level -->
            <!-- Profiles -->
            <li<?php
            if ($active == 'Profiles_list' || $active == 'Profiles_add') {
                echo ' class="open"';
            }
            ?> >
                <a class="dropdown-toggle" href="#">
                    <span class="fas fa-unlock-alt "></span> <?= __('Profile'); ?>
                    <b class="arrow fas fa-angle-down"></b>
                </a>
                <!-- thrid level -->
                <ul class="submenu">        
                    <li <?php
                    if ($active == 'Profiles_add')
                        echo ' class="active"';
                    ?> >
                            <?php
                            echo $this->Html->link(
                                    '<span class="glyphicon glyphicon-edit"></span>' . __('Add'), array(
                                'controller' => 'Profiles',
                                'action' => 'add'), array(
                                'escape' => false,
                                'class' => 'animated animated-short fadeInUp')
                            );
                            ?>
                    </li>
                    <li <?php
                    if ($active == 'Profiles_list')
                        echo ' class="active"';
                    ?> >
                            <?php
                            echo $this->Html->link(
                                    '<span class="glyphicon glyphicon-modal-window"></span>' . __('List'), array(
                                'controller' => 'Profiles',
                                'action' => 'index'
                                    ), array(
                                'escape' => false,
                                'class' => 'animated animated-short fadeInUp')
                            );
                            ?>
                    </li><!-- End third level -->
                </ul> 
            </li><!-- End second level -->
            <!-- Users -->
            <li<?php
            if ($active == 'Users_list' || $active == 'Users_add') {
                echo ' class="open"';
            }
            ?> >
                <a class="dropdown-toggle" href="#">
                    <span class="fas fa-wrench"></span> <?= __('Settings'); ?>
                    <b class="arrow fas fa-angle-down"></b>
                </a>
                <!-- thrid level -->
                <ul class="submenu">        
                    <li <?php
                    if ($active == 'Configs_add')
                        echo ' class="active"';
                    ?> >
                            <?php
                            echo $this->Html->link(
                                    '<span class="glyphicon glyphicon-edit"></span>' . __('Add'), array(
                                'controller' => 'Configs',
                                'action' => 'add'), array(
                                'escape' => false,
                                'class' => 'animated animated-short fadeInUp')
                            );
                            ?>
                    </li>
                    <li <?php
                    if ($active == 'Configs_list')
                        echo ' class="active"';
                    ?> >
                            <?php
                            echo $this->Html->link(
                                    '<span class="glyphicon glyphicon-modal-window"></span>' . __('List'), array(
                                'controller' => 'Configs',
                                'action' => 'index'
                                    ), array(
                                'escape' => false,
                                'class' => 'animated animated-short fadeInUp')
                            );
                            ?>
                    </li><!-- End third level -->
                </ul> 
            </li><!-- End second level -->
        </ul>
    </li>
</ul><!-- /.nav-list -->
