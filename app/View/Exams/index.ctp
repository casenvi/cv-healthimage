<?php
echo $this->Html->css(array(
    'datatables.net/css/jquery.dataTables.css',
    'cv/Exam.css',
    'cornerstone/cornerstone.min.css'
));
//page specific plugin scripts
echo $this->Html->script(array(
    'datatables.net/jquery.dataTables.js',
    'datatables.net-buttons/dataTables.buttons.js',
    'datatables.net-buttons/buttons.flash.js',
    'datatables.net-buttons/buttons.html5.js',
    'datatables.net-buttons/buttons.print.js',
    'datatables.net-buttons/buttons.colVis.js',
    'datatables.net-select/dataTables.select.js',
    'cv/Exam.js',
    'cornerstone/cornerstone.min.js',
    'cornerstone/cornerstoneMath.min.js',
    'cornerstone/cornerstoneTools.min.js',
    'cornerstone/dicomParser.min.js',
    'cornerstone/codecs/openJPEG-FixedMemory.js',
    'cornerstone/codecs/charLS-FixedMemory-browser.js',
    'cornerstone/codecs/jpegLossless.js',
    'cornerstone/codecs/jpeg.js',
    'cornerstone/codecs/pako.min.js',
    'cornerstone/cornerstoneWADOImageLoader.min.js',
    'cornerstone/dicomfile/uids.js',
    'cornerstone/cornerstoneWADOImageLoaderWebWorker.min.js',
    'bootbox.js/bootbox.min.js'
));

?>
<div class="page-header">
    <h3><?= __('Exams'); ?></h3>
</div><!-- /.page-header -->
<div class="row">
    <div class="col-xs-12">
        <div class="clearfix">
            <div class="pull-left tableTools">
            </div>
            <div class="pull-right tableTools-container">
            </div>
        </div>
        <div class="table-header">
            <br/>
        </div>
        <div class="space-6"></div>
        <div>
            <table class="table table-striped table-bordered table-hover" data-step="3" id="list_ajax">
                <thead>
                <tr>
                    <th> + / - </th>
                    <th> <?= __('Patient'); ?> </th>
                    <th> <?= __('Birthdate'); ?> </th>
                    <th> <?= __('Medical record'); ?> </th>
                    <th> <?= __('Last exam date'); ?> </th>
                    <th> <?= __('Last institution'); ?> </th>
                    <th> <?= __('Studies'); ?> </th>
                </tr>

                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    var image = "";
    var loaded = false;
    var imgID = "";
    cornerstoneWADOImageLoader.external.cornerstone = cornerstone;

    $(document).ready(function() {
        getExams();
    });
    $(document).on('shown.bs.modal', function() {
        loadAndViewImage("wadouri:"+image, imgID);
        image = "";
    });

    function modal(img, id) {
        image = img;
        imgID = id;
        $("#exam").modal('show');
    }

</script>
<div class="modal fade my-modal" id="exam" tabindex="-1" role="dialog" aria-labelledby="modalLabel2" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?= __('Exams view'); ?></h4>
            </div>
            <div class="modal-body" style="height:100%;">
                <div class="container-fluid" style="height:100%;">
                    <div class="row">
                        <div class="col-md-12" style="text-align: center">
                            <a id="enableWindowLevelTool" class="list-group-item active">
                                <i class="fa fa-adjust fa-2x"></i></br>
                                <?= __('WW/WC'); ?>
                            </a>
                            <a id="pan" class="list-group-item">
                                <i class="fas fa-arrows-alt fa-2x"></i></br>
                                <?= __('Pan'); ?>
                            </a>
                            <a id="zoom" class="list-group-item">
                                <i class="fas fa-search fa-2x"></i></br>
                                <?= __('Zoom'); ?>
                            </a>
                            <a id="rotate" class="list-group-item">
                                <i class="fas fa-sync fa-2x"></i></br>
                                <?= __('Rotate'); ?>
                            </a>
                            <a id="enableLength" class="list-group-item">
                                <i class="fas fa-ruler-horizontal fa-2x"></i></br>
                                <?= __('Length'); ?>
                            </a>
                            <a id="probe" class="list-group-item">
                                <i class="fas fa-pen fa-2x"></i></br>
                                <?= __('Probe'); ?>
                            </a>
                            <a id="circleroi" class="list-group-item">
                                <i class="fas fa-circle fa-2x"></i></br>
                                <?= __('Elliptical ROI'); ?>
                            </a>
                            <a id="rectangleroi" class="list-group-item">
                                <i class="fas fa-square fa-2x"></i></br>
                                <?= __('Rectangle ROI'); ?>
                            </a>
                            <a id="angle" class="list-group-item">
                                <i class="fas fa-ruler-combined fa-2x"></i></br>
                                <?= __('Angle'); ?>
                            </a>
                            <a id="highlight" class="list-group-item">
                                <i class="fas fa-highlighter fa-2x"></i></br>
                                <?= __('Highlight'); ?>
                            </a>
                            <a id="annotation" class="list-group-item">
                                <i class="fas fa-sticky-note fa-2x"></i></br>
                                <?= __('Annotation'); ?>
                            </a>
                            <a id="freehand" class="list-group-item">
                                <i class="fas fa-draw-polygon fa-2x"></i></br>
                                <?= __('Freeform ROI'); ?>
                            </a>
                            <a id="eraser" class="list-group-item">
                                <i class="fa fa-eraser fa-2x"></i></br>
                                <?= __('Eraser'); ?>
                            </a>
                            <a id="export" class="list-group-item">
                                <i class="fas fa-file-export fa-2x"></i></br>
                                <?= __('Export'); ?>
                            </a>
                            <a id="save" onclick="setStates('dicomImage')" class="list-group-item">
                                <i class="fas fa-save fa-2x"></i></br>
                                <?= __('Save'); ?>
                            </a>
                            <fieldset class="states">
                                <label for="savedStates"><?= __('Previous states');?></label>
                                <select id="savedStates">
                                    <option value="0"><?= __('Clear'); ?></option>
                                </select>
                            </fieldset>

                            <a id="restore" class="list-group-item">
                                <i class="fas fa-undo fa-2x"></i></br>
                                <?= __('Restore'); ?>
                            </a>
                        </div>
                    </div>
                    <div class="row" style="height:100%;">
                        <div class="col-md-8" style="height:90%; text-align: center;">
                            <div id="dicomImage" style="width:100%;height:100%;position:relative;color: white;display:inline-block;border-style:solid;border-color:black;" oncontextmenu="return false" class="disable-selection noIbar"
                                 unselectable="on" onselectstart="return false;" onmousedown="return false;">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <h5><?= __('Patient') ;?>: <span id="patient"></span></h5><br>
                            <h5><?= __('Medical record') ;?>: <span id="medical_record"></span></h5><br>
                            <span><?= __('Image ID') ;?>: </span><span id="imageID"></span><br>
                            <span><?= __('Samples Per Pixel') ;?>: </span><span id="samplesPerPixel">1</span><br>
                            <span><?= __('Photometric Interpretation') ;?>: </span><span id="photometricInterpretation"></span><br>
                            <span><?= __('Number Of Frames') ;?>: </span><span id="numberOfFrames"></span><br>
                            <span><?= __('Rows') ;?>: </span><span id="rows"></span><br>
                            <span><?= __('Columns') ;?>: </span><span id="columns"></span><br>
                            <span><?= __('Pixel Spacing') ;?>: </span><span id="pixelSpacing"></span><br>
                            <span><?= __('Row Pixel Spacing') ;?>: </span><span id="rowPixelSpacing"></span><br>
                            <span><?= __('Column Pixel Spacing') ;?>: </span><span id="columnPixelSpacing"></span><br>
                            <span><?= __('Bits Allocated') ;?>: </span><span id="bitsAllocated"></span><br>
                            <span><?= __('Bits Stored') ;?>: </span><span id="bitsStored"></span><br>
                            <span><?= __('High Bit') ;?>: </span><span id="highBit"></span><br>
                            <span><?= __('Pixel Representation') ;?>: </span><span id="pixelRepresentation"></span><br>
                            <span><?= __('WindowCenter') ;?>: </span><span id="windowCenter"></span><br>
                            <span><?= __('WindowWidth') ;?>: </span><span id="windowWidth"></span><br>
                            <span><?= __('RescaleIntercept') ;?>: </span><span id="rescaleIntercept"></span><br>
                            <span><?= __('Total Time') ;?>: </span><span id="totalTime"></span><br>
                            <span><?= __('Load Time') ;?>: </span><span id="loadTime"></span><br>
                            <span><?= __('Decode Time') ;?>: </span><span id="decodeTime"></span><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

