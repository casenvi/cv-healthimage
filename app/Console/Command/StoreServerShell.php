<?php
/**
 * Created by PhpStorm.
 * User: cassiovidal
 * Date: 17/09/18
 * Time: 16:43
 */

/*
 * Add command above in crontab where CAKEDIR is the folder instalation on health.
 *
 * @reboot php CAKEDIR/app/Console/cake.php -app CAKEDIR/app storeServer > CAKEDIR/app/tmp/logs/pacs.log
 *
 *
 */

class StoreServerShell extends AppShell {

    public $uses = array('Config', 'Patient', 'Study', 'Image');

    public function main()
    {
        try{
            $this->logger("Inicio");
            $config = json_decode($this->read(), true);
            $port = $config['port'];
            $dcmtkDir = $config['dcmtkDir'];
            chdir(dirname(__FILE__));
            $dir = substr(__DIR__, 0, -16);
            $storescp_cmd = $dcmtkDir . "/storescp -v -dhl -td 20 -ta 20 --fork " . // Be verbose, set timeouts, fork into multiple processes
                "-xf storescp.cfg Default " . // Our config file
                "-od $dir/tmp/ " . // Where to put images we receive
                "-xcr \" $dir/Console/Command/import.sh \"#p\" \"#f\" \"#c\" \"#a\"\" " . // Run this script with these args after image reception
                "$port "; // Listen on this port
            $this->logger($storescp_cmd);
            exec($storescp_cmd);
            $this->logger("final");

        }catch(Exception $e){
            $this->logger($e);
        }


    }

    public function import()
    {
        $this->logger("Start Import");
        $dir = (isset($this->args[0]) ? $this->args[0] : ''); // Directory our DICOM file is
        $file = (isset($this->args[1]) ? $this->args[1] : ''); // Filename of the DICOM file
        $sent_to_ae = (isset($this->args[2]) ? $this->args[2] : ''); // AE Title the image was sent to
        $sent_from_ae = ((isset($this->args[3]) ? $this->args[3] : '')); // AE Title the image was sent from

        $dcmtkDir = $this->Config->get_config('dcmtkDir');

        $this->logger("Model loaded");

        $store_path = substr(__DIR__, 0, -16) . "/webroot";

        // Lets make sure we were called correctly.
        if (!$file || !$dir || !$sent_to_ae || !$sent_from_ae) {
            $this->logger("Missing args: " . print_r($this->args, true));
            exit;
        }

        // Lets get the dicom_tag class ready to go so we can look at the data in the image
        $d = new dicom_tag;

        // Lets make sure the DICOM file exists before proceeding
        $d->file = "$dir/$file"; //
        if (!file_exists($d->file)) {
            $this->logger($d->file . ": does not exist");
            exit;
        }

        // Load the tags from the images
        $d->load_tags($dcmtkDir);

        // We're going to parse out some information from the DICOM file and store it in an array for easy finding
        $data = array(
            'Patient' => array(
                'firstname'         => '',
                'lastname'          => '',
                'birthdate'         => '0000-00-00',
                'medical_record'    => ''
            ),
            'Study' => array(
                    'appt_date'     => '',
                    'study_uid'     => '',
                    'study_desc'    => '',
                    'accession'     => '',
                    'history'       => '',
                    'institution'   => '',
                    'sent_from_ae'  => '',
                    'sent_to_ae'    => ''
            ),
            'Image' => array(
                    'series_number'         => '',
                    'instance_number'       => '',
                    'sop_instance'          => '',
                    'transfer_syntax'       => '',
                    'body_part_examined'    => '',
                    'image_date'            => '',
                    'modality'              => ''

            )
        );
        // patient data
        //list($data['Patient']['lastname'], $data['Patient']['firstname']) = explode('^', $d->get_tag('0010', '0010'));
        $data['Patient']['firstname'] = $d->get_tag('0010', '0010');
        $data['Patient']['medical_record'] = $d->get_tag('0010', '0020');
        $data['Patient']['birthdate'] = date('Y-m-d', strtotime($d->get_tag('0010', '0030')));

        // study data
        $data['Study']['appt_date'] = date('Y-m-d', strtotime($d->get_tag('0008', '0020')));
        // date to create folder list
        list($year, $month, $day) = explode('-', $data['Study']['appt_date']);
        $data['Study']['study_uid'] = $d->get_tag('0020', '000d');
        $data['Study']['study_desc'] = $d->get_tag('0008', '1030');
        $data['Study']['accession'] = $d->get_tag('0008', '0050');
        $data['Study']['history'] = $d->get_tag('0010', '21B0');
        $data['Study']['institution'] = $d->get_tag('0008', '0080');
        $data['Study']['sent_from_ae'] = $sent_from_ae;
        $data['Study']['sent_to_ae'] = $sent_to_ae;

        //image
        $data['Image']['series_number'] = $d->get_tag('0020', '0011');
        $data['Image']['instance_number'] = $d->get_tag('0020', '0013');
        $data['Image']['sop_instance'] = $d->get_tag('0008', '0018');
        $data['Image']['transfer_syntax'] = $d->get_tag('0002', '0010');
        $data['Image']['body_part_examined'] = $d->get_tag('0018', '0015');
        $data['Image']['image_date'] = date('Y-m-d G:i:s', strtotime(
            $d->get_tag('0008', '0023') . ' ' . $d->get_tag('0008', '0033')));
        $data['Image']['modality'] = $d->get_tag('0008', '0060');

        // Log that we received the image
        $this->logger("Received " . $data['Patient']['firstname'] . ' ' . $data['Patient']['lastname'] . " from $sent_to_ae -> $sent_from_ae");
        // For debug, lets log the entire $img array so we can see what's going on
        $this->logger(print_r($data, true));

        // Lets create a directory to keep our images in.
        // We're going to organize the images by the AE title they came from and the date they were taken.
        $webpath = "/Storage/$sent_from_ae/" . $year . "/" . $month . "/" . $day . "/" . $data['Study']['study_uid'];
        $store_dir = "$store_path/$webpath";

        if (!file_exists($store_dir)) {
            mkdir($store_dir, 0777, true);
            $this->logger("Created $store_dir");
        }

        //verify if new image is a duplicate.
        if (file_exists("$store_dir/$file.dcm")) {
            $this->logger("$store_dir/$file.dcm already exists, new file is probably a duplicate, ignoring.");
            unlink($d->file);
            exit;
        }

// Move our received image into the storage directory.
        rename($d->file, "$store_dir/$file.dcm");
        chmod("$store_dir/$file.dcm", 0666);
        $this->logger("Moved image to $store_dir/$file.dcm");
        $data['Image']['src'] = "$webpath/$file.dcm";
// Create thumb
        $d = new dicom_convert;
        $d->file = "$store_dir/$file.dcm";
        $d->dcm_to_tn($dcmtkDir);
        $data['Image']['thumb'] = "$webpath/$file.dcm_tn.jpg";

// Now that we've got the image headers parsed and the image moved to where we want to keep it, lets put the information
// we collected into a database

        $patient = $this->Patient->savePatient($data['Patient']);
        $data['Study']['patient_id'] = $patient['Patient']['id'];
        $study = $this->Study->saveStudy($data['Study']);
        $data['Image']['study_id'] = $study['Study']['id'];
        $this->Image->save($data['Image']);
        $this->logger("Saved into database");


    }
    // This function will log a message to a file
    private function logger($message, $type = 'storeServer') {
        $this->log($message, $type);
    }

    public function config()
    {
        chdir(dirname(__FILE__));
        $dir = substr(__DIR__, 0, -16)  . "/Console/Command/config.ini";
        $port = $this->Config->get_config('pacsPort');
        $dcmtkDir = $this->Config->get_config('dcmtkDir');
        $json = json_encode(array("port" => $port, "dcmtkDir" => $dcmtkDir));
        if(file_exists($dir))
            unlink($dir);
        file_put_contents($dir, $json);

    }

    private function read()
    {
        chdir(dirname(__FILE__));
        $dir = substr(__DIR__, 0, -16);
        if(file_exists($dir  . "/Console/Command/config.ini"))
        {
            return file_get_contents($dir . "/Console/Command/config.ini", true);
        }
        else{
            echo "Config file not existe. Execute $dir/Console/cake storeServer config and restart the server. \n";
            die();
        }
    }

}

/*
$d = new dicom_tag;
$d->file = 'SOME_IMAGE.dcm';
$d->load_tags();
$name = $d->get_tag('0010', '0010');
*/

class dicom_tag {

    var $tags = array();
    var $file = -1;

### LOAD DICOM TAGS FROM A FILE INTO AN ARRAY ($this->tags). $this->file is the filename of the image.
    function load_tags($dcmtkDir) {

        $file = $this->file;
        $dump_cmd = $dcmtkDir . "/dcmdump -M +L +Qn $file";
        exec($dump_cmd, $dump);

        if (!$dump) {
            return (0);
        }

        $this->tags = array();

        foreach ($dump as $line) {

            $ge = '';

            preg_match_all("/\((.*)\) [A-Z][A-Z]/", $line, $matches);
            if (isset($matches[1][0])) {
                $ge = $matches[1][0];
                if (!isset($this->tags["$ge"])) {
                    $this->tags["$ge"] = '';
                }
            }

            if (!$ge) {
                continue;
            }

            $found = 0;
            preg_match_all("/\[(.*)\]/", $line, $matches);
            if (isset($matches[1][0])) {
                $found = 1;
                $val = $matches[1][0];

                if (is_array($this->tags["$ge"])) { // Already an array
                    $this->tags["$ge"][] = $val;
                } else { // Create new array
                    $old_val = $this->tags["$ge"];
                    if ($old_val) {
                        $this->tags["$ge"] = array();
                        $this->tags["$ge"][] = $old_val;
                        $this->tags["$ge"][] = $val;
                    } else {
                        $this->tags["$ge"] = $val;
                    }
                }
            }

            if (is_array($this->tags["$ge"])) {
                $found = 1;
            }

            if (!$found) { // a couple of tags are not in [] preceded by =
                preg_match_all("/\=(.*)\#/", $line, $matches);
                if (isset($matches[1][0])) {
                    $found = 1;
                    $val = $matches[1][0];
                    $this->tags["$ge"] = rtrim($val);
                }
            }

            if (!$found) { // a couple of tags are not in []
                preg_match_all("/[A-Z][A-Z] (.*)\#/", $line, $matches);
                if (isset($matches[1][0])) {
                    $val = $matches[1][0];
                    if (strstr($val, '(no value available)')) {
                        $val = '';
                    }
                    $this->tags["$ge"] = rtrim($val);
                }
            }
        }
        return '';
    }

### AFTER load_tags() HAS BEEN CALLED, USE THIS TO GET A SPECIFIC TAG
    function get_tag($group, $element) {
        $val = '';
        if (isset($this->tags["$group,$element"])) {
            $val = $this->tags["$group,$element"];
        }
        return ($val);
    }
}

class dicom_convert
{

    var $file = '';
    var $jpg_file = '';
    var $tn_file = '';
    var $jpg_quality = 100;
    var $tn_size = 125;

### Convert a DICOM image to JPEG. $this->file is the filename of the image.
### $this->jpg_quality is an optional value (0-100) that'll set the quality of the JPEG produced
    function dcm_to_jpg($dcmtkDir)
    {

        $filesize = 0;

        $this->jpg_file = $this->file . '.jpg';

        $convert_cmd = $dcmtkDir . "/dcmj2pnm +oj +Jq " . $this->jpg_quality . " --use-window 1 \"" . $this->file . "\" \"" . $this->jpg_file . "\"";
        exec($convert_cmd, $out);

        if (file_exists($this->jpg_file)) {
            $filesize = filesize($this->jpg_file);
        }

        if ($filesize < 10) {
            $convert_cmd = $dcmtkDir . "/dcmj2pnm +Wm +oj +Jq " . $this->jpg_quality . " \"" . $this->file . "\" \"" . $this->jpg_file . "\"";
            exec($convert_cmd, $out);
        }

        return ($this->jpg_file);

    }

### Convert $this->file into a JPEG thumbnail.
### Optional $this->tn_size will let you change the width of the thumbnail produced
    function dcm_to_tn($dcmtkDir)
    {
        $filesize = 0;
        $this->tn_file = $this->file . '_tn.jpg';

        $convert_cmd = $dcmtkDir . "/dcmj2pnm +oj +Jq 75 +Sxv " . $this->tn_size . " --use-window 1 \"" . $this->file . "\" \"" . $this->tn_file . "\"";
        exec($convert_cmd, $out);

        if (file_exists($this->tn_file)) {
            $filesize = filesize($this->tn_file);
        }

        if ($filesize < 10) {
            $convert_cmd = $dcmtkDir . "/dcmj2pnm +Wm +oj +Jq 75 +Sxv  " . $this->tn_size . " \"" . $this->file . "\" \"" . $this->tn_file . "\"";
            exec($convert_cmd, $out);
        }

        return ($this->tn_file);
    }

}