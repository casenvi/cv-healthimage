<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Users
 *
 * @author cassio
 */
class UsersController extends AppController {var $components=array("Email","Session");
    var $helpers=array("Html","Form","Session");
    public function index(){
        if (in_array('60', $this->Auth->user('permissions'))){
            $this->layout = 'in';
            $this->set('active', 'Users_list');
            $this->set('way', __('Admin / Users / ') . __('List'));
            $this->set('user_id',$this->Auth->User('id'));
            $this->User->recursive = 0;
            $this->set('Users', $this->paginate());
            $retorno = $this->User->find('all',array(
                'order' => array('User.username'))); 
            $this->set('retorno',$retorno);
        }else{
            $this->Session->setFlash(__('You do not have access to this module'),'danger');
            return $this->redirect($this->Auth->redirectUrl('/Pages/dashboard'));
        }
    }
    public function add($id=null) {
        if (in_array('57', $this->Auth->user('permissions')) || in_array('58', $this->Auth->user('permissions'))){
            $this->layout = 'in';
            $this->set('active', 'Users_add');
            $this->set('way', __('Admin / Users / ') . __('Add'));
            $this->set('id', $id);
            $this->set('uid',$this->Auth->User('id'));
            $this->loadModel('Profile');
            $this->set('ProfilesList', $this->Profile->profilelist());
            $this->set('LanguageList', array(
               'por' => __('Portuguese'),
               'eng' => __('English')
            ));
            if($id!=null ){
                $this->User->id=$id;
                if (!$this->User->exists()) {
                $this->Session->setFlash(__('Non-existent User'),'danger');
                $this->redirect(array('action' => 'index'));
                }
                if($this->User->user_id!=null) {
                    $this->User->user_id=$id;
                }
            }
            if ($this->request->is('post')||$this->request->is('put')) {
                if ($this->request->data['User']['password'] == '') {
                    unset($this->request->data['User']['password']);
                }
                if ($id == null){
                   if(in_array('57', $this->Auth->user('permissions'))) {
                        $this->User->create();
                   }else{
                        $this->Session->setFlash(__('You do not have access to this module'),'danger');
                        return $this->redirect($this->Auth->redirectUrl('/Pages/dashboard'));
                    }
                }
                if ($this->User->save($this->request->data)) {
                    if ($id!=null){
                        $this->Session->setFlash(__('User') . ' ' . __('updated successfully'),'success');
                    }
                    else{
                        $this->Session->setFlash(__('User') . ' ' . __('created successfully'),'success');
                    }                
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('An error occurred on save. Try again'),'danger');
                }
            }else if($id!=null){
                if (in_array('58', $this->Auth->user('permissions'))){
                    $user=$this->User->read(); 
                    unset($user['User']['password']);
                    $this->set('id', $user['User']['id']);
                    $this->request->data=$user;
                }else{
                    $this->Session->setFlash(__('You do not have access to this module'),'danger');
                    return $this->redirect($this->Auth->redirectUrl('/Users'));
                }
            }
        }else{
            $this->Session->setFlash(__('You do not have access to this module'),'danger');
            return $this->redirect($this->Auth->redirectUrl('/Pages/dashboard'));
        }
    }
    public function remove($id=null){
        if (in_array('59', $this->Auth->user('permissions'))){
            if ($this->User->remove($id)){
                $this->Session->setFlash(__('User') . ' ' . __('removed successfully'),'success');
                return $this->redirect(array('action'=>'index'));
            }else{
                $this->Session->setFlash(__('An error occurred on remove. Try again'),'danger');
                return $this->redirect(array('action'=>'index'));
            }
        }else{
            $this->Session->setFlash(__('You do not have access to this module'),'danger');
            return $this->redirect($this->Auth->redirectUrl('/Users'));
        }
    }
    
    public function login() {
         $this->Session->setFlash('');
         if ($this->request->is('post') || $this->request->is('put')) {  
            if ($this->Auth->login()) {  
                //seta o cookie  
                if($this->request->data['User']['rememberMe']=='1'){ 
                    unset($this->request->data['User']['rememberMe']);
                    $this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);
                    $this->Cookie->write('rememberMeHealth', $this->request->data['User'], true, '2 weeks');
                }
                $this->Session->setFlash(__('Logged in successfully'), 'success');
                if ($this->Auth->redirectUrl){
                    return $this->Auth->redirectUrl();
                }
                return $this->redirect(array(
                    'controller' => 'Pages',
                    'action' => 'dashboard'
                ));
            } else {  
                $this->Session->setFlash(__('Username or password not found, please try again or make sure your account is enabled'), 'danger');
            }  
        }else{  
            if ($this->Cookie->check('rememberMeHealth')) {
                $this->__logRememberMe();
            }
        }
    }   
    public function logout() {
        $this->Cookie->destroy('rememberMeHealth');
        $this->redirect($this->Auth->logout());
    }
    protected function __logRememberMe(){  
        $cookie=$this->Cookie->read('rememberMeHealth');
        $user = $this->User->find('first', array(
                'conditions' => array(
                    'User.username' => $cookie['username'],
                    'User.password' => $cookie['password']
                )
         ));
         if (!$this->Auth->login($user['User'])) {
             $this->Cookie->destroy('rememberMeHealth');
            $this->redirect('/users/logout'); // destroy session & cookie
         }else{
            $this->set('user', $user);
            $this->redirect(array(
                    'controller' => 'Pages',
                    'action' => 'dashboard'
                ));
         }
    } 
    public function forgetpwd(){
        $this->User->recursive=-1;
        $error = 0;
        if(!empty($this->data)){
            if(empty($this->data['User']['username'])){
                $this->Session->setFlash(__('Not Set User'),'danger');
            }else{
                $error = 1;
            }
            if(empty($this->data['User']['email'])){
                $this->Session->setFlash(__('Not Set E-mail'),'danger');
            }else{
                $error = 1;
            }
            if ($error === 0){
                $email=$this->data['User']['email'];
                $user=$this->data['User']['user'];
                $fu=$this->User->find('first',array(
                    'conditions'=>array(
                        'User.email'=>$email, 
                        'User.username' =>$user
                )));
                if($fu){
                    if($fu['User']['enable']){
                        $key = Security::hash(String::uuid(),'sha512',true);
                        $hash=sha1($fu['User']['username'].rand(0,100));
                        $url = Router::url( array('controller'=>'users','action'=>'reset'), true ).'/'.$key.'#'.$hash;
                        $ms=$url;
                        $ms=wordwrap($ms,1000);
                        $fu['User']['tokenhash']=$key;
                        $this->User->id=$fu['User']['id'];
                        if($this->User->saveField('tokenhash',$fu['User']['tokenhash'])){
                            //============Email================//
                            /* SMTP Options */
                            $this->Email->smtpOptions = array(
                                    'port'=>'25',
                                    'timeout'=>'30',
                                    'host' => 'mail.cassiovidal.xyz',
                                    'username'=>'rememberme@cassiovidal.xyz',
                                    'password'=>'rememberme1q2w3e'
                                              );
                            $this->Email->template = 'resetpw';
                            $this->Email->from    = 'Lembrar senha <rememberme@cassiovidal.xyz>';
                            $this->Email->to      = $fu['User']['name'].'<'.$fu['Email'][0]['email'].'>';
                            $this->Email->subject = __('Reset your password');
                            $this->Email->sendAs = 'both';
                            $this->Email->delivery = 'smtp';
                            $this->set('ms', $ms);
                            $this->Email->send();
                            $this->set('smtp_errors', $this->Email->smtpError);
                            $this->Session->setFlash(__('Check your email to reset your password'), 'success');

                            //============EndEmail=============//
                        }else{
                            $this->Session->setFlash(__('An error occurred on password reset. Try again'),'danger');
                        }
                    }else{
                        $this->Session->setFlash(__( 'This account is disabled.'),'danger');
                    }
                }else{
                    $this->Session->setFlash(__( 'Account not found'),'danger');
                }
            }
        }
    }
    public function reset($token=null){
        //$this->layout="Login";
        $this->User->recursive=-1;
        if(!empty($token)){
            $u=$this->User->findBytokenhash($token);
            if($u){
                $this->User->id=$u['User']['id'];
                if(!empty($this->data)){
                    $this->User->data=$this->data;
                    $this->User->data['User']['username']=$u['User']['username'];
                    $new_hash=sha1($u['User']['username'].rand(0,100));//created token
                    $this->User->data['User']['tokenhash']=$new_hash;
                    if($this->User->validates(array('fieldList'=>array('password','password_confirm')))){
                        if($this->User->save($this->User->data)){
                            $this->Session->setFlash(__('Password reset successfully'));
                            $this->redirect('/');
                        }
                    }else{
                        $this->set('errors',$this->User->invalidFields());
                    }
                }
            }else{
                $this->Session->setFlash(__( 'Security token invalid. Generate new token'));
            }
        }else{
            $this->redirect('/');
        }
    }
}