<?php
/**
 * Created by PhpStorm.
 * User: cassiovidal
 * Date: 06/08/18
 * Time: 10:34
 */


class ExamsController extends AppController
{
    public function index()
    {
        if (in_array('232', $this->Auth->user('permissions'))) {
            $this->layout = 'in';
            $this->set('active', __('Exams'));
            $this->set('way', __('Exams'));

            $this->set('user_id', $this->Auth->User('id'));
        } else {
            $this->Session->setFlash(__('You do not have access to this module'), 'danger');
            return $this->redirect($this->Auth->redirectUrl('/Pages/dashboard'));
        }
    }

    public function getExams()
    {
        $this->layout = 'ajax';
        $this->loadModel('Patient');
        $data = $this->Patient->find('all', array(
            'recursive' => 0,
        ));
        foreach ($data as $key => $patient)
        {
            $data[$key]['Patient']['Study'] = $this->Patient->Study->find('all', array(
                   'conditions' => array(
                        'Study.patient_id' => $patient['Patient']['id']
                    )
            ));
            $lastExame = $this->Patient->query("SELECT appt_date, institution FROM health.studies where "
                    ."patient_id = {$patient['Patient']['id']} and id = (select max(id) from studies "
                    ."where patient_id = {$patient['Patient']['id']});");
            $data[$key]['Patient']['lastexam'] = $lastExame[0]['studies']['appt_date'];
            $data[$key]['Patient']['lastinstitution'] = $lastExame[0]['studies']['institution'];

        }
        $this->set('data', json_encode($data));

    }
}