<?php
/**
 * Created by PhpStorm.
 * User: cassiovidal
 * Date: 22/08/18
 * Time: 13:12
 */

class ViewStatesController extends AppController
{
    public function getViews($imageID){
        $this->layout = 'ajax';
        $data = $this->ViewState->find('all', array(
            'conditions' => array(
                'image_id' => $imageID
            ),
            'order' => array(
                'id'
            )
        ));
        $return = array(
            '0' => array(
                'description' => __('No states'),
                'state' => ''
            )
        );
        foreach ($data as $key => $value){
            $return[$key]['description'] = $value['ViewState']['description'];
            $return[$key]['state'] = $value['ViewState']['state'];
        }
        $this->set('data', json_encode($return));

    }
    public function setViews(){
        $this->layout = 'ajax';
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->request->data['user_id'] = $this->Auth->user('id');
            if ($this->ViewState->save( $this->request->data)){
                $this->set('data', __('State') . ' ' . __('created successfully'));
            }else{
                $this->set('data', __('An error occurred on save. Try again'));
            }
        }
    }
}