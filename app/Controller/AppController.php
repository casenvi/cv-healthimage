<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    var $helpers = array('Js' => array('Jquery'),
        'Html' => array('className' => 'BootstrapHtml'),
        'Form' => array('className' => 'BootstrapForm'),
        'Paginator' => array('className' => 'BootstrapPaginator')
    );
    public $components = array(
        'Session',
        'Cookie',
        'Auth' => array(
            'logoutRedirect' => array('controller' => 'Users', 'action' => 'login')
        )
    );

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('forgetpwd', 'reset', 'getExam');
        $user = $this->Auth->user();
        if ($user && !array_key_exists('person', $user) && array_key_exists('id', $user)) {
            $this->loadModel('User');
            $user = $this->User->find('first', array(
                'conditions' => array(
                    'User.id' => $user['id']
                )
            ));
            $this->loadModel('Profile');
            $this->Session->write('Auth.User.permissions', $this->Profile->userprofile($user['User']['profile_id']));
        } else {
            $this->loadModel('Profile');
            if (is_array($user) && array_key_exists('profile_id', $user)) {
                $this->Session->write('Auth.User.permissions', $this->Profile->userprofile($user['profile_id']));
            }
        }
        if (isset($user['User']['language'])){
            $this->Session->write('Config.language', $user['User']['language']);
        }else{
            $this->loadModel('Config');
            $cfg = $this->Config->find('all', array(
                'conditions' => array(
                    'description' => 'defaultLanguage'
                ),
                'fields' => array(
                    'value'
                )
            ));
            if (isset($cfg[0]['Config']['value'])){
                $this->Session->write('Config.language', $cfg[0]['Config']['value']);

            }else{
                $this->Session->write('Config.language', 'eng');
            }
        }
        if ($this->Session->check('Config.language')) {
            Configure::write('Config.language', $this->Session->read('Config.language'));
        }
        $this->set('user', $user);
        
        // mobile detect to links
        App::import('Vendor', 'MobileDetect/Mobile_Detect');
        $detect = new Mobile_Detect;
        if ($detect->isMobile()) {
            $this->set('mobile', 1);
        } else {
            $this->set('mobile', 0);
        }
        // #end Mobile Detect
    }

    function extractnumbers($string) {
        preg_match_all('/([\d]+)/', $string, $match);
        $number = NULL;
        foreach ($match[0] as $value):
            $number = $number . $value;
        endforeach;
        return $number;
    }

    function convertDateHour($date = null) {
        $date = str_replace('/', '-', $date);
        $date = date('Y-m-d H:i', strtotime($date));
        return $date;
    }

    function extractPeriod($date = null) {
        $pos = strpos($date, ' - ');
        $inidate = substr($date, 0, $pos);
        $enddate = substr($date, ($pos + 3));
        $return[0] = $inidate;
        $return[1] = $enddate;
        return $return;
    }

    public function upload($file = array(), $dir = 'tmp', $name = null) {
        $dir = WWW_ROOT . $dir . DS;
        if (($file['error'] != 0) and ( $file['size'] == 0)) {
            throw new NotImplementedException('Alguma coisa deu errado, o upload retornou erro ' . $file['error'] . ' e tamanho ' . $file['size']);
        }
        $this->check_dir($dir);
        if ($name != null) {
            $file_info = pathinfo($dir . $file['name']);
            $file['name'] = $name . '.' . $file_info['extension'];
        }
        $file = $this->check_name($file, $dir);
        $this->move_file($file, $dir);
        //return $dir.$file['name'];
        return $file['name'];
    }

    public function check_dir($dir) {
        App::uses('Folder', 'Utility');
        $folder = new Folder();
        if (!is_dir($dir)) {
            $folder->create($dir);
        }
    }

    public function check_name($file, $dir) {
        $file_info = pathinfo($dir . $file['name']);
        $file_name = $this->care_name($file_info['filename']) . '.' . $file_info['extension'];
        $count = 2;
        while (file_exists($dir . $file_name)) {
            $file_name = $this->care_name($file_info['filename']) . '-' . $count;
            $file_name .= '.' . $file_info['extension'];
            $count++;
        }
        $file['name'] = $file_name;
        return $file;
    }

    public function move_file($file, $dir) {
        App::uses('File', 'Utility');
        $arquivo = new File($file['tmp_name']);
        $arquivo->copy($dir . $file['name']);
        $arquivo->close();
    }

    public function care_name($file_name) {
        $file_name = strtolower(Inflector::slug($file_name, '-'));
        return $file_name;
    }

    function resolve($name) {
        // reads informations over the path
        $info = pathinfo($name);
        $filename = $info['filename'];
        $len = strlen($filename);
        // open the folder
        $dh = opendir($info['dirname']);
        if (!$dh) {
            return false;
        }
        // scan each file in the folder
        while (($file = readdir($dh)) !== false) {
            if (strncmp($file, $filename, $len) === 0) {
                return $file;
            }
        }
        // file not found
        closedir($dh);
        return false;
    }

}
