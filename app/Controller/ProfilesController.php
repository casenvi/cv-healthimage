<?php
/**
 * Description of Profiles
 * Referente ao cadastro de grupos de usuários
 * @author cassio
 */
class ProfilesController extends AppController {
    public $uses    = array('Module', 'Profile');
    public function index(){
        if (in_array('64', $this->Auth->user('permissions'))){
            $this->layout = 'in';
            $this->set('active', 'Profiles_list');
            $this->set('way', __('Admin / Profile / ') .  __('List'));
            $this->Profile->recursive = 0;
            $this->set('Profiles', $this->paginate());
            $retorno = $this->Profile->find('all', array(
                'conditions' => array('id !=' => 1))); 
            $this->set('retorno',$retorno);
        }else{
            $this->Session->setFlash(__('You do not have access to this module'),'danger');
            return $this->redirect($this->Auth->redirectUrl('/Pages/dashboard'));
        }    
    }
    public function add($id=null) {
        if (in_array('61', $this->Auth->user('permissions')) || in_array('62', $this->Auth->user('permissions'))){
            $this->layout = 'in';
            $this->set('active', 'Profiles_add');
            $this->set('way', __('Admin / Profile / ') .  __('Add'));
            $this->set('uid',$this->Auth->User('id'));
            $this->set('id', $id);
            $this->set('modules', $this->Module->modulelist());
            if($id!=null){
                $this->Profile->id=$id;
                if (!$this->Profile->exists()) {
                    throw new NotFoundException(__('Nonexistent') . ' ' . __('profile'));
                }
            }
            if ($this->request->is('post')||$this->request->is('put')) {
                if ($id == null){
                    if(in_array('61', $this->Auth->user('permissions'))) {
                       $this->Profile->create();
                    }else{
                        $this->Session->setFlash(__('You do not have access to this module'),'danger');
                        return $this->redirect($this->Auth->redirectUrl('/Profiles'));
                    }
                }
                $this->request->data['Profile']['name'] = ucwords(strtolower($this->request->data['Profile']['name']));
                if ($this->Profile->save($this->request->data)) {
                    if ($id!=null){
                        $this->Session->setFlash(__('Profile' ) . ' ' . __('updated successfully'),'success');
                    }
                    else{
                        $this->Session->setFlash(__('Profile' ) . ' ' . __('created successfully'),'success');
                    }                
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('An error occurred on save. Try again'),'danger');
                }
            }
            else if($id!=null){
                if (in_array('62', $this->Auth->user('permissions'))){
                    $profile=$this->Profile->read(); 
                    $this->request->data=$profile;
                }else{
                    $this->Session->setFlash(__('You do not have access to this module'),'danger');
                    return $this->redirect($this->Auth->redirectUrl('/Profiles'));
                }
            }else{
                $this->request->data['Module'] = array();
            }
        }else{
            $this->Session->setFlash(__('You do not have access to this module'),'danger');
            return $this->redirect($this->Auth->redirectUrl('/Pages/dashboard'));
        }
    }
    public function remove($id=null){
        if (in_array('63', $this->Auth->user('permissions'))){
            if($this->Profile->remove($id)){
                $this->Session->setFlash(__('Profile' ) . ' ' . __('removed successfully'),'success');
            }else{
                $this->Session->setFlash(__('An error occurred on remove. Try again'),'danger');
            }
            return $this->redirect(array('action'=>'index'));
        }else{
            $this->Session->setFlash(__('You do not have access to this module'),'danger');
            return $this->redirect($this->Auth->redirectUrl('/Profiles'));
        }
    }    
}