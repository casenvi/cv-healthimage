<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppController', 'Controller');

/**
 * CakePHP ConfigController
 * @author casen
 */
class ConfigsController extends AppController {

    public function index(){
        if (in_array('209', $this->Auth->user('permissions'))){
            $this->layout = 'in';
            $this->set('active', 'Configs_list');
            $this->set('way', __('Admin / Settings / ') . __('List'));
            $this->set('user_id',$this->Auth->User('id'));
            $this->Config->recursive = 0;
            $this->set('Configs', $this->paginate());
            $retorno = $this->Config->find('all',array(
                'order' => array('Config.description'))); 
            $this->set('retorno',$retorno);
        }else{
            $this->Session->setFlash(__('You do not have access to this module'),'danger');
            return $this->redirect($this->Auth->redirectUrl('/Pages/dashboard'));
        }
    }
    public function add($id=null) {
        if (in_array('206', $this->Auth->user('permissions')) || in_array('207', $this->Auth->user('permissions'))){
            $this->layout = 'in';
            $this->set('active', 'Configs_add');
            $this->set('way', __('Admin / Settings / ') . __('Add'));
            $this->set('id', $id);
            $this->set('uid',$this->Auth->User('id'));
            $this->loadModel('Profile');
            $this->set('ProfilesList', $this->Profile->profilelist());
            if($id!=null ){
                $this->Config->id=$id;
                if (!$this->Config->exists()) {
                $this->Session->setFlash(__('Non-existent') . ' ' . __('Setting'),'danger');
                $this->redirect(array('action' => 'index'));
                }
                if($this->Config->user_id!=null) {
                    $this->Config->user_id=$id;
                }
            }
            if ($this->request->is('post')||$this->request->is('put')) {
                if ($this->request->data['Config']['password'] == '') {
                    unset($this->request->data['Config']['password']);
                }
                if ($id == null){
                    if(in_array('206', $this->Auth->user('permissions'))) {
                        $this->Config->create();
                    }else{
                        $this->Session->setFlash(__('You do not have access to this module'),'danger');
                        return $this->redirect($this->Auth->redirectUrl('/Pages/dashboard'));
                    }
                }
                if ($this->Config->save($this->request->data)) {
                    if ($id!=null){
                        $this->Session->setFlash(__('Setting') . ' ' . __('updated successfully'),'success');
                    }
                    else{
                        $this->Session->setFlash(__('Setting') . ' ' . __('created successfully'),'success');
                    }                
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('An error occurred on save. Try again'),'danger');
                }
            }else if($id!=null){
                if (in_array('207', $this->Auth->user('permissions'))){
                    $user=$this->Config->read(); 
                    unset($user['Config']['password']);
                    $this->set('id', $user['Config']['id']);
                    $this->request->data=$user;
                }else{
                    $this->Session->setFlash(__('You do not have access to this module'),'danger');
                    return $this->redirect($this->Auth->redirectUrl('/Configs'));
                }
            }
        }else{
            $this->Session->setFlash(__('You do not have access to this module'),'danger');
            return $this->redirect($this->Auth->redirectUrl('/Pages/dashboard'));
        }
    }
    public function remove($id=null){
        if (in_array('208', $this->Auth->user('permissions'))){
            if ($this->Config->remove($id)){
                $this->Session->setFlash(__('Setting') . ' ' . __('removed successfully'),'success');
                return $this->redirect(array('action'=>'index'));
            }else{
                $this->Session->setFlash(__('An error occurred on remove. Try again'),'danger');
                return $this->redirect(array('action'=>'index'));
            }
        }else{
            $this->Session->setFlash(__('You do not have access to this module'),'danger');
            return $this->redirect($this->Auth->redirectUrl('/Configs'));
        }
    }

}
