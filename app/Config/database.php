<?php
class DATABASE_CONFIG {

	public $default = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'health',
		'password' => 'health',
		'database' => 'health',
		'prefix' => '',
		'encoding' => 'utf8'
	);

}
