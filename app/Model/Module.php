<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Module
 *
 * @author casen
 */
class Module extends AppModel{
     public $name ='Module';
     public $hasAndBelongsToMamny = array(
         'Profile'=> array(
                'className' => 'Profile',
                'joinTable' => 'modules_profiles',
                'foreignKey' => 'modules_id',
                'associationForeignKey' => 'profiles_id',
                'unique' => true,
            )
     );
     public function modulelist(){
         $modules = $this->find('list',array(
             'group' => 'name'
         ));
         foreach ($modules as $value) {
            $teste = $this->find('list',array(
                'conditions' => array(
                    'name' => $value
                ),
                'fields' => 'action'
            ));
            $return[$value] = $teste; 
         }
         return $return;
     }
}
