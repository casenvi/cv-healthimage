<?php
/**
 * Description of Profile
 *
 * @author cassio
 */
App::uses('AuthComponent', 'Controller/Component');

class Profile extends AppModel{
    public $name ='Profile';
    public $hasMany = array(
        'User'
        );
    public $hasAndBelongsToMany = array(
        'Module' => array(
                'className' => 'Module',
                'joinTable' => 'modules_profiles',
                'foreignKey' => 'profiles_id',
                'associationForeignKey' => 'modules_id',
                'unique' => true,
            )
    );
    public $validate = array(
        'name' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Profile not blank'
                ),
            'tamanho' => array(
                'rule'=> array('minLength',3),
                'message'=>'Profile too short'
            ),
            'unico' => array(
                'rule'=> array('isUnique',3),
                'message'=>'Profile in use'
            )
        )
    );
    public function profilelist() {
        $profile = $this->find('list',array(
            'conditions'=>array(
                 'Status' => '1')));         
        return $profile;
    }
    public function userprofile($profile_id = null) {
        $return = null;
        if ($profile_id){
            $permission = $this->query(
                    'SELECT modules_id FROM modules_profiles where profiles_id = "'.$profile_id.'"'
                    );
            foreach ($permission as $key => $value){
                $return[$key] = $value['modules_profiles']['modules_id'];
            }
            
        }
        return $return;
    }
    public function remove($id) {
        $this->id = $id; 
        if($this->exists()){
            if($this->delete()){
                return TRUE;   
            }else{
                return FALSE;
            }
        }else{
            return FALSE;
        }            
    }  
}
