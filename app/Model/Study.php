<?php
/**
 * Created by PhpStorm.
 * User: cassiovidal
 * Date: 18/09/18
 * Time: 13:22
 */

class Study extends AppModel{
    public $name ='Study';

    public $hasMany = array(
        'Image'
    );

    public function saveStudy($data)
    {
        $study = $this->find('first', array(
            'conditions' => array(
                'Study.study_uid' => $data['study_uid']
            )
        ));
        if (isset($study['Study']['id'])){
            return $study;
        }
        else {
            return $this->save($data);
        }
    }

}