<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Configs
 *
 * @author cassio
 */
App::uses('AuthComponent', 'Controller/Component');

class Config extends AppModel{
    public $name ='Config';    
    public $validate = array(
        'description' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Description not blank'
                ),            
            'unico' => array(
                'rule'=> array('isUnique',3),
                'message'=>'Description in use'
            )
        )
    );
    public function remove($id) {
        $this->id = $id; 
        if($this->exists() && $this->id != '1'){
            if($this->delete()){
                return TRUE;   
            }else{
                return FALSE;
            }
        }else{
            return FALSE;
        }            
    }
    
    public function get_config($description = null){
        if ($description){
            $return = $this->find('first', array(
                'conditions' => array(
                    'description' => $description                    
                ),
                'fields' => array(
                    'value'
                )
            ));
            return $return['Config']['value'];
        }
    }
    
}
