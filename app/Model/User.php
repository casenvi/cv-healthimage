<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Users
 *
 * @author cassio
 */
App::uses('AuthComponent', 'Controller/Component');

class User extends AppModel{
    public $name ='User';
    public $belongsTo = array(
        'Profile',
        );
    public $hasMany = array(
    );
    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Username is empty'
                ),
            'tamanho' => array(
                'rule'=> array('minLength',3),
                'message'=>'Username too short'
            ),
            'unico' => array(
                'rule'=> array('isUnique',3),
                'message'=>'Username in use'
            )
        ),
        'password' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Password is empty'
            )
        ),
        'confirma' => array(
            'required' => array(
                'rule' => array('passwordconfirm','password'),
                'message' => 'Confirm Password not equals to password'
            )
        )
    );
    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        }
        return true;
    }
    public function passwordconfirm($data, $controlField) {  
        if (!isset($this->data[$this->alias][$controlField])) { 
            if (current($data)==''){
                return true;
            }
            trigger_error('O campo de comparação de senha não foi enviado');  
            return false;  
        }  
        $field = key($data);  
        $password = current($data);  
        $controlPassword = $this->data[$this->alias][$controlField];  
        if ($password !== $controlPassword) {  
            $this->invalidate($controlField, 'Repita a senha!');  
            return false;  
        }  
        return true;  
    }
    public function remove($id) {
        $this->id = $id; 
        if($this->exists() && $this->id != '1'){
            if($this->delete()){
                return TRUE;   
            }else{
                return FALSE;
            }
        }else{
            return FALSE;
        }            
    }
    public function userlist() {
        $return = $this->find('all', array(
            'conditions' => array(
                'User.status' => '1'
            ),
            'fields' => array(
                'User.id',
                'Person.name'
            )
        ));
        foreach ($return as $value) {
            $user[$value['User']['id']] = $value['Person']['name'];
        }
        return $user;
    }
}
