<?php
/**
 * Created by PhpStorm.
 * User: cassiovidal
 * Date: 22/08/18
 * Time: 13:21
 */

class ViewStates extends AppModel
{
    public $name = 'ViewStates';

    public $hasMany = array(
        'User'
    );

    public function remove($id) {
        $this->id = $id;
        if($this->exists()){
            if($this->delete()){
                return TRUE;
            }else{
                return FALSE;
            }
        }else{
            return FALSE;
        }
    }

}