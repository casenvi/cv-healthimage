<?php
/**
 * Created by PhpStorm.
 * User: cassiovidal
 * Date: 18/09/18
 * Time: 13:22
 */

class Patient extends AppModel{
    public $name ='Patient';

    public $hasMany = array(
        'Study'
    );

    public function savePatient($data)
    {
        $patient = $this->find('first', array(
            'conditions' => array(
                'Patient.medical_record' => $data['medical_record']
            )
        ));
        if (isset($patient['Patient']['id'])){
            return $patient;
        }
        else {
            return $this->save($data);
        }
    }
    

}