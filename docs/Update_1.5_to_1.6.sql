-- MySQL Workbench Synchronization
-- Generated: 2018-09-24 11:45
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: cassio vidal

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER SCHEMA `health_1.5`  DEFAULT CHARACTER SET utf8  DEFAULT COLLATE utf8_general_ci ;

ALTER TABLE `health_1.5`.`profiles` 
DROP FOREIGN KEY `fk_profiles_users1`;

ALTER TABLE `health_1.5`.`configs` 
CHARACTER SET = utf8 , COLLATE = utf8_general_ci ;

CREATE TABLE IF NOT EXISTS `health_1.5`.`images` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `study_id` INT(11) NOT NULL,
  `series_number` INT(11) NOT NULL,
  `instance_number` INT(11) NOT NULL,
  `sop_instance` VARCHAR(255) CHARACTER SET 'latin1' NOT NULL,
  `transfer_syntax` VARCHAR(100) CHARACTER SET 'latin1' NOT NULL,
  `body_part_examined` VARCHAR(100) CHARACTER SET 'latin1' NOT NULL,
  `image_date` DATETIME NOT NULL,
  `modality` VARCHAR(4) CHARACTER SET 'latin1' NOT NULL,
  `src` VARCHAR(255) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `thumb` VARCHAR(255) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `study_seq` (`study_id` ASC),
  CONSTRAINT `fk_images_studies1`
    FOREIGN KEY (`study_id`)
    REFERENCES `health_1.5`.`studies` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `health_1.5`.`patients` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `firstname` VARCHAR(100) CHARACTER SET 'utf8mb4' NOT NULL,
  `lastname` VARCHAR(100) CHARACTER SET 'utf8mb4' NULL DEFAULT NULL,
  `birthdate` DATE NULL DEFAULT NULL,
  `medical_record` VARCHAR(20) CHARACTER SET 'utf8mb4' NULL DEFAULT NULL,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;

ALTER TABLE `health_1.5`.`profiles` 
ADD UNIQUE INDEX `id_UNIQUE` (`id` ASC),
DROP INDEX `id` ;
;

CREATE TABLE IF NOT EXISTS `health_1.5`.`studies` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `appt_date` DATE NOT NULL,
  `study_uid` VARCHAR(100) CHARACTER SET 'latin1' NOT NULL,
  `study_desc` VARCHAR(100) CHARACTER SET 'latin1' NOT NULL,
  `accession` VARCHAR(25) CHARACTER SET 'latin1' NOT NULL,
  `history` VARCHAR(255) CHARACTER SET 'latin1' NOT NULL,
  `institution` VARCHAR(255) CHARACTER SET 'latin1' NOT NULL,
  `sent_from_ae` VARCHAR(25) CHARACTER SET 'latin1' NOT NULL,
  `sent_to_ae` VARCHAR(25) CHARACTER SET 'latin1' NOT NULL,
  `patient_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_studies_patients1_idx` (`patient_id` ASC),
  CONSTRAINT `fk_studies_patients1`
    FOREIGN KEY (`patient_id`)
    REFERENCES `health_1.5`.`patients` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;

ALTER TABLE `health_1.5`.`view_states` 
CHARACTER SET = utf8 , COLLATE = utf8_general_ci ,
DROP COLUMN `image_id`,
ADD COLUMN `image_id` INT(11) NOT NULL AFTER `user_id`,
ADD INDEX `fk_view_states_images1_idx` (`image_id` ASC);
;

DROP TABLE IF EXISTS `health_1.5`.`i18n` ;

ALTER TABLE `health_1.5`.`users` 
ADD CONSTRAINT `fk_users_profiles1`
  FOREIGN KEY (`profile_id`)
  REFERENCES `health_1.5`.`profiles` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `health_1.5`.`view_states` 
ADD CONSTRAINT `fk_view_states_images1`
  FOREIGN KEY (`image_id`)
  REFERENCES `health_1.5`.`images` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
