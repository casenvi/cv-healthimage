-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema health_1.5
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema health_1.5
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `health_1.5` DEFAULT CHARACTER SET utf8mb4 ;
USE `health_1.5` ;

-- -----------------------------------------------------
-- Table `health_1.5`.`configs`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `health_1.5`.`configs` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(255) NOT NULL,
  `value` LONGTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `description_UNIQUE` (`description` ASC),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `health_1.5`.`i18n`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `health_1.5`.`i18n` (
  `id` INT(10) NOT NULL AUTO_INCREMENT,
  `locale` VARCHAR(6) NOT NULL,
  `model` VARCHAR(255) NOT NULL,
  `foreign_key` INT(10) NOT NULL,
  `field` VARCHAR(255) NOT NULL,
  `content` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `locale` (`locale` ASC),
  INDEX `model` (`model` ASC),
  INDEX `row_id` (`foreign_key` ASC),
  INDEX `field` (`field` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `health_1.5`.`modules`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `health_1.5`.`modules` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(105) NOT NULL,
  `action` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 210
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `health_1.5`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `health_1.5`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `password` VARCHAR(45) NOT NULL,
  `username` VARCHAR(45) NOT NULL,
  `lastlogon` DATETIME NULL DEFAULT NULL,
  `created` DATETIME NULL DEFAULT NULL,
  `modified` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT NULL,
  `modified_by` INT(11) NULL DEFAULT NULL,
  `profile_id` INT(11) NULL DEFAULT 1,
  `status` TINYINT(1) NULL DEFAULT 1,
  `language` VARCHAR(3) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_users_profiles1_idx` (`profile_id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `health_1.5`.`profiles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `health_1.5`.`profiles` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(105) NOT NULL,
  `status` TINYINT(1) NULL DEFAULT 1,
  `created` DATETIME NULL DEFAULT NULL,
  `modifiqued` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT NULL,
  `modifiqued_by` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id` (`id` ASC),
  CONSTRAINT `fk_profiles_users1`
    FOREIGN KEY (`id`)
    REFERENCES `health_1.5`.`users` (`profile_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `health_1.5`.`modules_profiles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `health_1.5`.`modules_profiles` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `modules_id` INT(11) NOT NULL,
  `profiles_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_modules_has_profiles_profiles1_idx` (`profiles_id` ASC),
  INDEX `fk_modules_profiles_modules1_idx` (`modules_id` ASC),
  CONSTRAINT `fk_modules_profiles_modules1`
    FOREIGN KEY (`modules_id`)
    REFERENCES `health_1.5`.`modules` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_modules_profiles_profiles1`
    FOREIGN KEY (`profiles_id`)
    REFERENCES `health_1.5`.`profiles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1349
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `health_1.5`.`view_states`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `health_1.5`.`view_states` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `state` LONGTEXT NULL,
  `description` VARCHAR(45) NULL,
  `user_id` INT NOT NULL,
  `image_id` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_view_states_users1_idx` (`user_id` ASC),
  CONSTRAINT `fk_view_states_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `health_1.5`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Data for table `health_1.5`.`modules`
-- -----------------------------------------------------
START TRANSACTION;
USE `health_1.5`;
INSERT INTO `health_1.5`.`modules` (`id`, `name`, `action`) VALUES (57, 'UsuÃ¡rios', 'Adicionar');
INSERT INTO `health_1.5`.`modules` (`id`, `name`, `action`) VALUES (58, 'UsuÃ¡rios', 'Editar');
INSERT INTO `health_1.5`.`modules` (`id`, `name`, `action`) VALUES (59, 'UsuÃ¡rios', 'Excluir');
INSERT INTO `health_1.5`.`modules` (`id`, `name`, `action`) VALUES (60, 'UsuÃ¡rios', 'Visualizar');
INSERT INTO `health_1.5`.`modules` (`id`, `name`, `action`) VALUES (61, 'Perfil', 'Adicionar');
INSERT INTO `health_1.5`.`modules` (`id`, `name`, `action`) VALUES (62, 'Perfil', 'Editar');
INSERT INTO `health_1.5`.`modules` (`id`, `name`, `action`) VALUES (63, 'Perfil', 'Excluir');
INSERT INTO `health_1.5`.`modules` (`id`, `name`, `action`) VALUES (64, 'Perfil', 'Visualizar');
INSERT INTO `health_1.5`.`modules` (`id`, `name`, `action`) VALUES (206, 'ConfiguraÃ§Ãµes', 'Adicionar');
INSERT INTO `health_1.5`.`modules` (`id`, `name`, `action`) VALUES (207, 'ConfiguraÃ§Ãµes', 'Editar');
INSERT INTO `health_1.5`.`modules` (`id`, `name`, `action`) VALUES (208, 'ConfiguraÃ§Ãµes', 'Excluir');
INSERT INTO `health_1.5`.`modules` (`id`, `name`, `action`) VALUES (209, 'ConfiguraÃ§Ãµes', 'Visualizar');
INSERT INTO `health_1.5`.`modules` (`id`, `name`, `action`) VALUES (232, 'Exames', 'Visualizar');

COMMIT;


-- -----------------------------------------------------
-- Data for table `health_1.5`.`users`
-- -----------------------------------------------------
START TRANSACTION;
USE `health_1.5`;
INSERT INTO `health_1.5`.`users` (`id`, `password`, `username`, `lastlogon`, `created`, `modified`, `created_by`, `modified_by`, `profile_id`, `status`, `language`) VALUES (3, 'e1a56ba8dfe8e6843769df0f22f8b90061b3eb69', 'admin', NULL, '2018-08-06 12:38:42', '2018-08-06 12:38:42', NULL, NULL, 3, 1, NULL);

COMMIT;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;