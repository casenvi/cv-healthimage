

Health Image 
================

Health Image is an open source project with a goal to deliver a complete web based medical imaging platform with:

* PACS server
* DICOM Image view by web. 
* User profile to permissions
* Control Access by user

To this version implement to PACS server using the DCMTK and DICOM php class and the cakePHP 2.9 to provide the web view and controls
 of access. To view the images,  we using the Cornerstone Core and CornerstoneTools component to view and edit images. 

Community
---------
Have questions?  Send me a email support@cassiovidal.com.

Requeriments
-------
To install the Health Image is very simple. Do you will need:
* PHP 7.0 or heigther;
* Mysql or other SGDB supported by Cake PHP 2.9
* php json extension.
* DCMTK toolkit

This requiriments can is installed by composer. 
 
To install DCMTK into centos follow these steps:

1. Install libChartLS using `yum install -y CharLS`
2. Download the package in `http://li.nux.ro/download/nux/dextop/el7/x86_64/dcmtk-3.6.0-16.el7.nux.x86_64.rpm`
3. Install using  `sudo rpm -i -v dcmtk-3.6.0-16.el7.nux.x86_64.rpm`
4. After do you need know the install path, using `sudo find / | grep "storescp"`, usually `/usr/bin`

Install
-------
After requeriments installed and the website installed do you need:
 1. Download and extract the system in your system. 
 2. Configure a website to this folder. We recommend Apache 2.4.
 3. Configure the database access in Config/database.php 
 4. Install the database running `docs/database.sql`
 5. Add the storeServer to crontab follow these steps:
    1. `crontab -e`
    2. add this line: `@reboot php CAKEDIR/app/Console/cake.php -app CAKEDIR/app storeServer > CAKEDIR/app/tmp/logs/pacs.log` 
    where CAKEDIR is your path of instalation
 6. Access the webpage to user Admin password Admin and will to configuration:
    1. Configure:
        1. `dcmtkDir`
        2.  and `pacsPort`. Add the allow firewall rule for this port. 
 7. Run the command `CAKEDIR/app/Console/cake storeServer config` to generate config file.
 8. Reboot your server, now this PACS server already running. 
 
Configure your medical device
-----------------------------
To configure your device do you need:
* **AETITLE** : any vaue
* **IP**      : server ip address
* **Port**    : the port configured in pacsPort

The Health Image accept any protocol and any AETITLE.  

Key Features
------------

 * HTML5/Javascript based library to easily add interactive medical images to web applications
 * Serves as a foundation to build more complex medical imaging applications from - enterprise viewer, report viewer, etc.
 * Supports all HTML5 based browsers including mobile, tablet and desktop
 * Displays all common medical image formats (e.g. 8 bit grayscale, 16 bit grayscale, RGB color)
 * High performance image display
 * Retrieval of images from different systems with different protocols via Image Loader plugin design
 * API support for changing viewport properties (e.g. ww/wc, zoom, pan, invert)


Links
=====

[2014 comp.protocols.dicom thread that started this project](https://groups.google.com/forum/#!topic/comp.protocols.dicom/_2fMh69GdAM)

[CornerstoneTools](https://github.com/cornerstonejs/cornerstoneTools) - A library of common tools that can be used with Cornerstone

[CornerstoneWADOImageLoader](https://github.com/cornerstonejs/cornerstoneWADOImageLoader) - A Cornerstone Image Loader that works with WADO-URI, WADO-RS and DICOM P10 files

[CornerstoneWebImageLoader](https://github.com/cornerstonejs/cornerstoneWebImageLoader) - A Cornerstone Image Loader that works with PNG and JPEG files

[dicomParser](https://github.com/cornerstonejs/dicomParser) - A JavaScript library designed to parse DICOM for web browsers

[DCMTK](https://dicom.offis.de/dcmtk.php.en) - A collection of libraries and applications implementing large parts the DICOM standard.

[DICOM PHP CLASS](https://deanvaughan.org/wordpress/dicom-php-class/) - A PHP class that lets you work with DICOM files within PHP programs.


Code Contributors
=================

I welcome pull requests, please see FAQ below for guidance on this.

FAQ
===

_Why did you decide to license this system using the open source MIT license?_
 
 The main reason this system is released as open source is that I believe that medical imaging in particular can do a 
 lot more to improve patient outcomes but the cost of doing so is prohibitive. Making this system open source removes the cost barrier.
 
 The old adage a picture is worth a thousand words is very true in medical imaging. When a patient is going through a 
 disease process, they often face fear and confusion. Medical terminology amplifies these issues as it is hard to understand 
 and therefore disempowering. Medical imaging allows a mysterious health issue to be visualized and therefore brings a 
 level of understanding that just can't be accomplished via textual information found in lab or radiology reports. By 
 helping a patient (and its supporting friends/family) connect with the disease visually through images, it is believed 
 that fear, anxiety and confusion will all be reduced which will increase optimism and therefore patient outcomes.

_Why doesn't this library support older browsers like IE8?_

Much of the performance in this library is possible due to utilizing modern web features such as HTML5 canvas,
high performance javascript engines and WebGL.  These feature are not avaialble in IE8 and there are no suitable
polyfills available.  Sorry, upgrade your browser.

_I would like to contribute code - how do I do this?_

Fork the repository, make your change and submit a pull request.

_Any guidance on submitting changes?_

While I do appreciate code contributions, I will not merge it unless it meets the following criteria:

* Functionality is appropriate for the repository.  Consider posting on the forum if you are not sure
* Code quality is acceptable.  I don't have coding standards defined, but make sure it passes ESLint and looks like the 
rest of the code in the repository.
* Quality of design is acceptable.  This is a bit subjective so you should consider posting on the forum for specific guidance

I will provide feedback on your pull request if it fails to meet any of the above.

Please consider separate pull requests for each feature as big pull requests are very time consuming to understand.
It is highly probably that I will reject a large pull request due to the time it would take to comprehend.

_Will you add feature XYZ for me?_

If it is in the roadmap, I intend to implement it some day - probably when I actually need it.  If you really need something 
now and are willing to pay for it, send me a email [cassio@cassiovidal.com](mailto:cassio@cassiovidal.com).

Copyright
=========

Copyright 2018 Cassio Vidal [cassio@cassiovidal.com](mailto:cassio@cassiovidal.com)

[license-image]: http://img.shields.io/badge/license-MIT-blue.svg?style=flat
[license-url]: LICENSE
