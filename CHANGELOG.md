## 1.6.0 (September 20, 2018)
In this version the following items were computed:
 - Exams 
    - Birth date into patient list;
    - Change layout of exam view;
    - Add rotate tool on exam view; 
 - PACS Server
    - Removed support at Dicoogle;
    - Added PACS server by DCMTK based on the Dicom php class; 

## 1.5.0 (September 03, 2018)
In this version the following items were computed:
 - Removal of marking of points
 - Cleaning and organization of files
 - Sorting of the list of exams by the date of the exam
 - Added remark in the picture notes
 - Exporting images to JPEG
 - Recording of notes in layers
 - Standardization of the system language and i18n enablement

